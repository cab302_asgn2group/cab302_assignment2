package asgn2Manifests;

import java.util.ArrayList;
import java.util.List;

import asgn2Codes.ContainerCode;
import asgn2Containers.FreightContainer;
import asgn2Exceptions.ManifestException;

/**
 * A class for managing a container ship's cargo manifest.  It
 * allows freight containers of various types to be loaded and
 * unloaded, within various constraints.
 * <p>
 * In particular, the ship's captain has set the following rules
 * for loading of new containers:
 * <ol>
 * <li>
 * New containers may be loaded only if doing so does not exceed
 * the ship's weight limit.
 * </li>
 * <li>
 * New containers are to be loaded as close to the bridge as possible.
 * (Stack number zero is nearest the bridge.)
 * </li>
 * <li>
 * A new container may be added to a stack only if doing so will
 * not exceed the maximum allowed stack height.
 * <li>
 * A new container may be loaded only if a container with the same
 * code is not already on board.
 * </li>
 * <li>
 * Stacks of containers must be homogeneous, i.e., each stack must
 * consist of containers of one type (general,
 * refrigerated or dangerous goods) only.
 * </li>
 * </ol>
 * <p>
 * Furthermore, since the containers are moved by an overhead
 * crane, a container can be unloaded only if it is on top of
 * a stack.
 *  
 * @author Thuan Nguyen N7195508
 * @version 1.0
 */
public class CargoManifest {
	private List<ArrayList<FreightContainer>> manifest;
	private Integer numStacks;
	private Integer maxHeight;
	private Integer maxWeight;
	private Integer currentWeight = 0;
	
	/**
	 * Constructs a new cargo manifest in preparation for a voyage.
	 * When a cargo manifest is constructed the specific cargo
	 * parameters for the voyage are set, including the number of
	 * stack spaces available on the deck (which depends on the deck configuration
	 * for the voyage), the maximum allowed height of any stack (which depends on
	 * the weather conditions expected for the
	 * voyage), and the total weight of containers allowed onboard (which depends
	 * on the amount of ballast and fuel being carried).
	 * 
	 * @author Thuan Nguyen N7195508
	 * @param numStacks the number of stacks that can be accommodated on deck
	 * @param maxHeight the maximum allowable height of any stack
	 * @param maxWeight the maximum weight of containers allowed on board 
	 * (in tonnes)
	 * @throws ManifestException if negative numbers are given for any of the
	 * parameters
	 */
    public CargoManifest(Integer numStacks, Integer maxHeight, Integer maxWeight)
            throws ManifestException {
        if (numStacks < 0 || maxHeight < 0 || maxWeight < 0) {
            throw new ManifestException("Invalid Values. Enter a positive integer.");
        }

        this.numStacks = numStacks;
        this.maxHeight = maxHeight;
        this.maxWeight = maxWeight;
        manifest = new ArrayList<ArrayList<FreightContainer>>();
        for (int i = 0; i < numStacks; i++) {
            manifest.add(new ArrayList<FreightContainer>());
        }
    }

	/**
	 * Loads a freight container onto the ship, provided that it can be
	 * accommodated within the five rules set by the captain.
	 * 
	 * @author Thuan Nguyen
	 * @param newContainer the new freight container to be loaded
	 * @throws ManifestException if adding this container would exceed
	 * the ship's weight limit; if a container with the same code is
	 * already on board; or if no suitable space can be found for this
	 * container
	 */
    public void loadContainer(FreightContainer newContainer) throws ManifestException {
        if (this.whichStack(newContainer.getCode()) != null) {
            throw new ManifestException("Container " + newContainer.getCode().toString()
                    + " is already on board");
        }
        if (currentWeight + newContainer.getGrossWeight() > maxWeight) {
            throw new ManifestException("Ship cannot handle any more weight");
        }

        boolean loadSuccessful = false;
        Integer i = 0;
        while (!loadSuccessful && i < manifest.size()) {
            if (manifest.get(i).isEmpty()) {
                manifest.get(i).add(newContainer);
                currentWeight += newContainer.getGrossWeight();
                loadSuccessful = true;
            } else if ((manifest.get(i).size() < this.maxHeight)
                    && (manifest.get(i).get(manifest.get(i).size() - 1).getClass()
                        == newContainer.getClass())) {
                manifest.get(i).add(newContainer);
                currentWeight += newContainer.getGrossWeight();
                loadSuccessful = true;
            }
            i++;
        }

        if (!loadSuccessful) {
            throw new ManifestException("Cannot add " + newContainer.getCode().toString()
                    + "\nCheck the weight of cargo or remaining space for the container type.");
        }
    }

	/**
	 * Unloads a particular container from the ship, provided that
	 * it is accessible (i.e., on top of a stack).
	 * 
	 * @author Thuan Nguyen
	 * @param containerId the code of the container to be unloaded
	 * @throws ManifestException if the container is not accessible because
	 * it's not on the top of a stack (including the case where it's not on board
	 * the ship at all)
	 */
    public void unloadContainer(ContainerCode containerId) throws ManifestException {
        Integer stackPoint = this.whichStack(containerId);
        if (stackPoint == null) {
            throw new ManifestException("Container not on board: " + containerId.toString());
        }

        Integer height = this.manifest.get(stackPoint).size();
        if (!this.manifest.get(stackPoint).get(height - 1).getCode().equals(containerId)) {
            throw new ManifestException("Cannot remove container: " + containerId.toString());
        }

        currentWeight -= this.manifest.get(stackPoint).get(height - 1).getGrossWeight();
        this.manifest.get(stackPoint).remove(height - 1);
    }

	
	/**
	 * Returns which stack holds a particular container, if any.  The
	 * container of interest is identified by its unique
	 * code.  Constant <code>null</code> is returned if the container is
	 * not on board.
	 * 
	 * @author Thuan Nguyen
	 * @param queryContainer the container code for the container of interest
	 * @return the number of the stack with the container in it, or <code>null</code>
	 * if the container is not on board
	 */
    public Integer whichStack(ContainerCode queryContainer) {
        Integer containerStack = null;
        for (Integer i = 0; i < this.manifest.size(); i++) {
            for (Integer j = 0; j < manifest.get(i).size(); j++) {
                if (manifest.get(i).get(j).getCode().equals(queryContainer)) {
                    return i;
                }
            }
        }
        return containerStack;
    }
	
	/**
	 * Returns how high in its stack a particular container is.  The container of
	 * interest is identified by its unique code.  Height is measured in the
	 * number of containers, counting from zero.  Thus the container at the bottom
	 * of a stack is at "height" 0, the container above it is at height 1, and so on.
	 * Constant <code>null</code> is returned if the container is
	 * not on board.
	 * 
	 * @author Thuan Nguyen
	 * @param queryContainer the container code for the container of interest
	 * @return the container's height in the stack, or <code>null</code>
	 * if the container is not on board
	 */
	public Integer howHigh(ContainerCode queryContainer) {
        // code here is the similar to whichStack but is used because calling
        // whichStack already requires two loops; one to identify the column and
        // the other to identify the row. We could use whichStack here to identify
        // the column but that would waste a bit of resources to go through two loops
        // to find the correct stack, then another loop to find the height.
        Integer containerHeight = null;
        for (Integer i = 0; i < this.numStacks; i++) {
            for (Integer j = 0; j < manifest.get(i).size(); j++) {
                if (manifest.get(i).get(j).getCode().equals(queryContainer)) {
                    return j;
                }
            }
        }
        return containerHeight;
    }


	/**
	 * Returns the contents of a particular stack as an array,
	 * starting with the bottommost container at position zero in the array.
	 * 
	 * @author Thuan Nguyen
	 * @param stackNo the number of the stack of interest
	 * @return the stack's freight containers as an array
	 * @throws ManifestException if there is no such stack on the ship
	 */
    public FreightContainer[] toArray(Integer stackNo) throws ManifestException {
        if (stackNo >= manifest.size()) {
            throw new ManifestException("No containers in stack: " + stackNo.toString());
        }
        if (manifest.get(stackNo).isEmpty()) {
            return null;
        }
        return manifest.get(stackNo).toArray(new FreightContainer[manifest.get(stackNo).size()]);
    }

	
	/* ***** toString methods added to support the GUI ***** */
	
    public String toString(ContainerCode toFind) {
        // Some variables here are used and not declared. You can work it out
        String toReturn = "";
        for (int i = 0; i < manifest.size(); ++i) {
            ArrayList<FreightContainer> currentStack = manifest.get(i);
            toReturn += "|";
            for (int j = 0; j < currentStack.size(); ++j) {
                if (toFind != null && currentStack.get(j).getCode().equals(toFind))
                    toReturn += "|*" + currentStack.get(j).getCode().toString() + "*|";
                else
                    toReturn += "| " + currentStack.get(j).getCode().toString() + " |";
            }
            if (currentStack.size() == 0)
                toReturn += "|  ||\n";
            else
                toReturn += "|\n";
        }
        return toReturn;
    }

	@Override
	public String toString() {
		return toString(null);
	}
	
//	/**
//	 * Used to assist with debugging - to display positions of containers and stacks
//	 * @author Thuan Nguyen
//	 */
//    public void printManifest() {
//        System.out.println("Number of Stacks: " + numStacks);
//        for (Integer i = 0; i < manifest.size(); i++) {
//            System.out.print("Stack size: " + manifest.get(i).size() + " ");
//            System.out.println("Stack num: " + i.toString());
//            for (Integer j = 0; j < manifest.get(i).size(); j++) {
//                System.out.print(manifest.get(i).get(j).getCode().toString() + " ");
//            }
//
//            System.out.println();
//        }
//    }
}
