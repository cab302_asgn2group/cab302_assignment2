package asgn2Tests;

/* Some valid container codes used in the tests below:
 * INKU2633836
 * KOCU8090115
 * MSCU6639871
 * CSQU3054389
 */

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Codes.ContainerCode;
import asgn2Exceptions.InvalidCodeException;
import asgn2Exceptions.InvalidContainerException;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.FreightContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;

/**
 * @author Thuan Nguyen - N7195508
 */
public class ContainerTests {
    // Default constants and variables used to test ContainerCode
    final String DEF_CODE = "INKU2633836";
    ContainerCode code;

    // Default constants and variables used for testing various
    // FreightContainers
    final Integer CONT_WEIGHT = 20;
    final Integer CONT_HEIGHT = 4;
    final Integer CONT_TEMP = 20;
    final Integer CONT_CATEGORY = 5;
    final Integer CONT_MIN_CATEGORY = 1;
    final Integer CONT_MAX_CATEGORY = 9;

    FreightContainer fCont;
    DangerousGoodsContainer dCont;
    GeneralGoodsContainer gCont;
    RefrigeratedContainer rCont;

    // Default constants used for testing CargoManifest
    final Integer MANIFEST_MAXWEIGHT = 30;
    final Integer MANIFEST_MINWEIGHT = 4;

    ///// Tests for ContainerCode /////
    /**
     * Tests construction of a <code>ContainerCode</code> with a valid code.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Before
    @Test
    public void test_ContainerCode() throws InvalidCodeException {
        code = new ContainerCode(DEF_CODE);
    }

    /**
     * Tests if <code>ContainerCode</code> throws an exception when random code is invalid
     *
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidCode() throws InvalidCodeException {
        code = new ContainerCode("K7AU1234566");
    }

    /**
     * Tests if <code>ContainerCode</code> throws an exception when serial is invalid
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidSerial_A_1() throws InvalidCodeException {
        code = new ContainerCode("AAAUA000000");
    }
    
    /**
     * Tests if <code>ContainerCode</code> throws an exception when serial is invalid
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidSerial_A_2() throws InvalidCodeException {
        code = new ContainerCode("AAAU00000A0");
    }
    
    /**
     * Tests if <code>ContainerCode</code> throws an exception when category is invalid
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidCategory_0() throws InvalidCodeException {
        code = new ContainerCode("INK02633836");
    }

    /**
     * Tests if <code>ContainerCode</code> throws an exception when category is invalid
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidCategory_J() throws InvalidCodeException {
        code = new ContainerCode("INKJ2633835");
    }

    /**
     * Tests if <code>ContainerCode</code> throws an exception when category is invalid
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidCategory_Z() throws InvalidCodeException {
        code = new ContainerCode("INKZ2633831");
    }

    /**
     * Tests if <code>ContainerCode</code> throws an exception when check digit is incorrect.
     *  
     * @author Thuan Nguyen - N7195508 
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidCheckDigit() throws InvalidCodeException {
        code = new ContainerCode("INKU2633831");
    }
    
    /**
     * Tests if <code>ContainerCode</code> throws an exception when check digit is 0.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidCheckDigit_0() throws InvalidCodeException {
        code = new ContainerCode("INKU2633830");
    }

    /**
     * Tests if <code>ContainerCode</code> throws an exception when the owner code has an invalid character.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidOwner() throws InvalidCodeException {
        code = new ContainerCode("K7AU1234568");
    }


    /**
     * Tests if <code>ContainerCode</code> throws exception for invalid owner code. <br>
     * Test on the basis that A = 0
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidOwner_0_1() throws InvalidCodeException {
        code = new ContainerCode("0AAU0000000");
    }
    
    /**
     * Tests if <code>ContainerCode</code> throws exception for invalid owner code. <br>
     * Test on the basis that A = 0
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidOwner_0_2() throws InvalidCodeException {
        code = new ContainerCode("A0AU0000000");
    }
    
    /**
     * Tests if <code>ContainerCode</code> throws exception for invalid owner code. <br>
     * Test on the basis that A = 0
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidOwner_0_3() throws InvalidCodeException {
        code = new ContainerCode("AA0U0000000");
    }
    
    /**
     * Tests if <code>ContainerCode</code> throws an exception when the owner code has an invalid character.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test(expected = InvalidCodeException.class)
    public void test_ContainerCode_invalidOwner_0_4() throws InvalidCodeException {
        code = new ContainerCode("AAaU0000000");
    }
    
    
    /**
     * Tests if <code>toString</code> has the correct value.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test
    public void test_toString() throws InvalidCodeException {
        final String TESTCODE = "INKU2633836";
        assertTrue(code.toString().equals(TESTCODE));
    }

    /**
     * Tests if the two <code>toString</code> can be compared.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test
    public void test_toString_twoStringsEqual() throws InvalidCodeException {
        final String TESTCODE = "INKU2633836";
        ContainerCode code2 = new ContainerCode(TESTCODE);
        assertTrue(code.toString().equals(code2.toString()));
    }

    /**
     * Test if <code>toString</code> returns the correct string.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test
    public void test_toString_twoStringsNotEqual() throws InvalidCodeException {
        final String TESTCODE = "KOCU8090115";
        ContainerCode code2 = new ContainerCode(TESTCODE);
        assertFalse(code.toString().equals(code2.toString()));
    }

    /**
     * Test if <code>equals</code> return true when code is equal.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test
    public void test_equals_true() throws InvalidCodeException {
        final String TESTCODE = "INKU2633836";
        ContainerCode code2 = new ContainerCode(TESTCODE);
        assertTrue(code.equals(code2));
    }

    /**
     * Test if <code>equals</code> return false when code is not equal.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidCodeException
     */
    @Test
    public void test_equals_false() throws InvalidCodeException {
        final String TESTCODE = "KOCU8090115";
        ContainerCode code2 = new ContainerCode(TESTCODE);
        assertFalse(code.equals(code2));
    }

    ///// Tests for Containers /////
    /**
     * Test constructors for all Container classes under the normal working conditions.
     *
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test
    public void test_FreightContainer_constructors() throws InvalidContainerException {
        final Integer falseWeight = 15;

        fCont = new DangerousGoodsContainer(code, CONT_WEIGHT, CONT_CATEGORY);
        assertEquals(CONT_WEIGHT, fCont.getGrossWeight());
        assertTrue(code.equals(fCont.getCode()));
        assertNotEquals(falseWeight, fCont.getGrossWeight());

        fCont = new GeneralGoodsContainer(code, CONT_WEIGHT);
        assertEquals(CONT_WEIGHT, fCont.getGrossWeight());
        assertTrue(code.equals(fCont.getCode()));
        assertNotEquals(falseWeight, fCont.getGrossWeight());

        fCont = new RefrigeratedContainer(code, CONT_WEIGHT, CONT_TEMP);
        assertEquals(CONT_WEIGHT, fCont.getGrossWeight());
        assertTrue(code.equals(fCont.getCode()));
        assertNotEquals(falseWeight, fCont.getGrossWeight());
    }

    /**
     * Tests if all containers returns the correct <code>ContainerCode</code> assigned to
     * them.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     * @throws InvalidCodeException
     */
    @Test
    public void test_FreightContainer_getCode() throws InvalidContainerException,
            InvalidCodeException {
        ContainerCode code2 = new ContainerCode("KOCU8090115");
        ContainerCode code3 = new ContainerCode("MSCU6639871");
        ContainerCode code4 = new ContainerCode("CSQU3054389");

        fCont = new DangerousGoodsContainer(code2, CONT_WEIGHT, CONT_CATEGORY);
        assertTrue(code2.equals(fCont.getCode()));
        assertFalse(code.equals(fCont.getCode()));

        fCont = new GeneralGoodsContainer(code3, CONT_WEIGHT);
        assertTrue(code3.equals(fCont.getCode()));
        assertFalse(code.equals(fCont.getCode()));

        fCont = new RefrigeratedContainer(code4, CONT_WEIGHT, CONT_TEMP);
        assertTrue(code4.equals(fCont.getCode()));
        assertFalse(code.equals(fCont.getCode()));
    }

    /**
     * Tests for an exception when <code>FreightContainer</code> instantiated by
     * <code>DangerousGoodsContainer</code> is overweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_FreightContainer_overweightDG_by1() throws InvalidContainerException {
        fCont = new DangerousGoodsContainer(code, MANIFEST_MAXWEIGHT + 1, CONT_CATEGORY);
    }

    /**
     * Tests for an exception when <code>FreightContainer</code> instantiated by
     * <code>GeneralGoodsContainer</code> is overweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_FreightContainer_overweightGG_by1() throws InvalidContainerException {
        fCont = new GeneralGoodsContainer(code, MANIFEST_MAXWEIGHT + 1);
    }

    /**
     * Tests for an exception when <code>FreightContainer</code> instantiated by
     * <code>RefrigeratedContainer</code> is overweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_FreightContainer_overweightRC_by1() throws InvalidContainerException {
        fCont = new RefrigeratedContainer(code, MANIFEST_MAXWEIGHT + 1, CONT_TEMP);
    }

    /**
     * Tests for an exception when <code>DangerousGoodsContainer</code> is overweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_DangerousGoodsContainer_overweight_by1() throws InvalidContainerException {
        dCont = new DangerousGoodsContainer(code, MANIFEST_MAXWEIGHT + 1, CONT_CATEGORY);
    }

    /**
     * Tests for an exception when <code>GeneralGoodsContainer</code> is overweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_GeneralGoodsContainer_overweight_by1() throws InvalidContainerException {
        gCont = new GeneralGoodsContainer(code, MANIFEST_MAXWEIGHT + 1);
    }

    /**
     * Tests for an exception when <code>RefrigeratedContainer</code> is overweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_RefrigeratedContainer_overweight_by1() throws InvalidContainerException {
        fCont = new RefrigeratedContainer(code, MANIFEST_MAXWEIGHT + 1, CONT_TEMP);
    }

    /**
     * Tests for an exception when <code>FreightContainer</code> instantiated by
     * <code>DangerousGoodsContainer</code> is underweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_FreightContainer_underweightDG_by1() throws InvalidContainerException {
        fCont = new DangerousGoodsContainer(code, MANIFEST_MINWEIGHT - 1, CONT_CATEGORY);
    }

    /**
     * Tests for an exception when <code>FreightContainer</code> instantiated by
     * <code>GeneralGoodsContainer</code> is underweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_FreightContainer_underweightGG_by1() throws InvalidContainerException {
        fCont = new GeneralGoodsContainer(code, MANIFEST_MINWEIGHT - 1);
    }

    /**
     * Tests for an exception when <code>FreightContainer</code> instantiated by
     * <code>RefrigeratedContainer</code> is underweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_FreightContainer_underweightRC_by1() throws InvalidContainerException {
        fCont = new RefrigeratedContainer(code, MANIFEST_MINWEIGHT - 1, CONT_TEMP);
    }

    /**
     * Tests for an exception when <code>DangerousGoodsContainer</code> is underweight by
     * 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_DangerousGoodsContainer_underweight_by1() throws InvalidContainerException {
        dCont = new DangerousGoodsContainer(code, MANIFEST_MINWEIGHT - 1, CONT_TEMP);
    }

    /**
     * Tests for an exception when <code>GeneralGoodsContainer</code> is underweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_GeneralGoodsContainer_underweight_by1() throws InvalidContainerException {
        gCont = new GeneralGoodsContainer(code, MANIFEST_MINWEIGHT - 1);
    }

    /**
     * Tests for an exception when <code>RefrigeratedContainer</code> is underweight by 1
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test(expected = InvalidContainerException.class)
    public void test_RefrigeratedContainer_underweight_by1() throws InvalidContainerException {
        fCont = new RefrigeratedContainer(code, MANIFEST_MINWEIGHT - 1, CONT_TEMP);
    }

    /**
     * Tests for a range of valid weight ranges for <code>FreightContainer</code> when
     * instantiated with <code>DangerousGoodsContainer</code>.<br>
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test
    public void test_FreightContainer_validWeightRangesDGC() throws InvalidContainerException {
        for (Integer i = MANIFEST_MINWEIGHT; i <= MANIFEST_MAXWEIGHT; i++) {
            fCont = new DangerousGoodsContainer(code, i, CONT_CATEGORY);
            assertEquals(i, fCont.getGrossWeight());
        }
    }

    /**
     * Tests for a range of valid weight ranges for <code>FreightContainer</code> when
     * instantiated with <code>GeneralGoodsContainer</code>.<br>
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test
    public void test_FreightContainer_validWeightRangesGGC() throws InvalidContainerException {
        for (Integer i = MANIFEST_MINWEIGHT; i <= MANIFEST_MAXWEIGHT; i++) {
            fCont = new GeneralGoodsContainer(code, i);
            assertEquals(i, fCont.getGrossWeight());
        }
    }

    /**
     * Tests for a range of valid weight ranges for <code>FreightContainer</code> when
     * instantiated with <code>RefrigeratedContainer</code>.<br>
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test
    public void test_FreightContainer_validWeightRangesRC() throws InvalidContainerException {
        for (Integer i = MANIFEST_MINWEIGHT; i <= MANIFEST_MAXWEIGHT; i++) {
            fCont = new RefrigeratedContainer(code, i, i);
            assertEquals(i, fCont.getGrossWeight());
        }
    }

    /**
     * Tests for a range of valid weight values for <code>DangerousGoodsContainer</code>
     * when instantiated with <code>DangerousGoodsContainer</code>.<br>
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test
    public void test_DangerousGoodsContainer_validWeightRanges() throws InvalidContainerException {
        for (Integer i = MANIFEST_MINWEIGHT; i <= MANIFEST_MAXWEIGHT; i++) {
            for (Integer j = CONT_MIN_CATEGORY; j < CONT_MAX_CATEGORY; j++) {
                dCont = new DangerousGoodsContainer(code, i, j);
                assertEquals(i, dCont.getGrossWeight());
                assertEquals(j, dCont.getCategory());
            }
        }
    }

    /**
     * Tests for a range of valid weight values for <code>GeneralGoodsContainer</code>
     * when instantiated with <code>GeneralGoodsContainer</code>.<br>
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test
    public void test_GeneralGoodsContainer_validWeightRanges() throws InvalidContainerException {
        for (Integer i = MANIFEST_MINWEIGHT; i <= MANIFEST_MAXWEIGHT; i++) {
            gCont = new GeneralGoodsContainer(code, i);
            assertEquals(i, gCont.getGrossWeight());
        }
    }

    /**
     * Tests for a range of valid weight values for <code>RefrigeratedContainer</code>
     * when instantiated with <code>RefrigeratedContainer</code>.<br>
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test
    public void test_RefrigeratedContainer_validWeightRanges() throws InvalidContainerException {
        for (Integer i = MANIFEST_MINWEIGHT; i <= MANIFEST_MAXWEIGHT; i++) {
            rCont = new RefrigeratedContainer(code, i, i + 10);
            assertEquals(i, rCont.getGrossWeight());
            assertEquals(i + 10, rCont.getTemperature().intValue());
        }
    }

    /**
     * Tests for a range of overweight values for <code>GeneralGoodsContainer</code> to
     * throw exceptions when instantiated with <code>GeneralGoodsContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_GeneralGoodsContainer_invalidOverweightRanges() {
        final Integer RANGE = 1000;
        for (Integer i = MANIFEST_MAXWEIGHT + 1; i <= RANGE; i++) {
            try {
                gCont = new GeneralGoodsContainer(code, i);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of underweight values for <code>GeneralGoodsContainer</code> to
     * throw exceptions when instantiated with <code>GeneralGoodsContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_GeneralGoodsContainer_invalidUnderweightRanges() {
        final Integer RANGE = -1000;
        for (Integer i = MANIFEST_MINWEIGHT - 1; i >= RANGE; i--) {
            try {
                gCont = new GeneralGoodsContainer(code, i);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of overweight values for <code>DangerousGoodsContainer</code> to
     * throw exceptions when instantiated with <code>DangerousGoodsContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_DangerousGoodsContainer_invalidOverweightRanges() {
        final Integer RANGE = 1000;
        for (Integer i = MANIFEST_MAXWEIGHT + 1; i <= RANGE; i++) {
            try {
                dCont = new DangerousGoodsContainer(code, i, CONT_CATEGORY);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of underweight values for <code>DangerousGoodsContainer</code> to
     * throw exceptions when instantiated with <code>DangerousGoodsContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_DangerousGoodsContainer_invalidUnderweightRanges() {
        final Integer RANGE = -1000;
        for (Integer i = MANIFEST_MINWEIGHT - 1; i >= RANGE; i--) {
            try {
                dCont = new DangerousGoodsContainer(code, i, CONT_CATEGORY);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of overweight values for <code>RefrigeratedContainer</code> to
     * throw exceptions when instantiated with <code>RefrigeratedContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_RefrigeratedContainer_invalidOverweightRanges() {
        final Integer RANGE = 1000;
        for (Integer i = MANIFEST_MAXWEIGHT + 1; i <= RANGE; i++) {
            try {
                rCont = new RefrigeratedContainer(code, i, CONT_TEMP);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of underweight values for <code>RefrigeratedContainer</code> to throw
     * exceptions when instantiated with <code>RefrigeratedContaienr</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_RefrigeratedContainer_invalidUnderweightRanges() {
        final Integer RANGE = -1000;
        for (Integer i = MANIFEST_MINWEIGHT - 1; i >= RANGE; i--) {
            try {
                rCont = new RefrigeratedContainer(code, i, CONT_TEMP);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of overweight values for <code>FreightContainer</code> to throw exceptions
     * when instantiated with <code>GeneralGoodsContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_FreightContainerGGC_invalidOverweightRanges() {
        final Integer RANGE = 1000;
        for (Integer i = MANIFEST_MAXWEIGHT + 1; i <= RANGE; i++) {
            try {
                fCont = new GeneralGoodsContainer(code, i);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of underweight values for <code>FreightContainer</code> to throw
     * exceptions when instantiated with <code>GeneralGoodsContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_FreightContainerGGC_invalidUnderweightRanges() {
        final Integer RANGE = -1000;
        for (Integer i = MANIFEST_MINWEIGHT - 1; i >= RANGE; i--) {
            try {
                fCont = new GeneralGoodsContainer(code, i);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of overweight values for <code>FreightContainer</code> to throw
     * exceptions when instantiated with <code>DangerousGoodsContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_FreightContainerDGC_invalidOverweightRanges() {
        final Integer RANGE = 1000;
        for (Integer i = MANIFEST_MAXWEIGHT + 1; i <= RANGE; i++) {
            try {
                fCont = new DangerousGoodsContainer(code, i, CONT_CATEGORY);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of underweight values for <code>FreightContainer</code> to throw
     * exceptions when instantiated with <code>DangerousGoodsContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_FreightContainerDGC_invalidUnderweightRanges() {
        final Integer RANGE = -1000;
        for (Integer i = MANIFEST_MINWEIGHT - 1; i >= RANGE; i--) {
            try {
                fCont = new DangerousGoodsContainer(code, i, CONT_CATEGORY);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of overweight values for <code>FreightContainer</code> to throw
     * exceptions when instantiated with <code>RefrigeratedContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_FreightContainerRC_invalidOverweightRanges() {
        final Integer RANGE = 1000;
        for (Integer i = MANIFEST_MAXWEIGHT + 1; i <= RANGE; i++) {
            try {
                fCont = new RefrigeratedContainer(code, i, CONT_TEMP);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for a range of underweight values for <code>FreightContainer</code> to throw exceptions
     * when instantiated with <code>RefrigeratedContainer</code>.<br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void test_FreightContainerRC_invalidUnderweightRanges() {
        final Integer RANGE = -1000;
        for (Integer i = MANIFEST_MINWEIGHT - 1; i >= RANGE; i--) {
            try {
                fCont = new RefrigeratedContainer(code, i, CONT_TEMP);
                fail("Failed at Weight: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for all valid category values in <code>DangerousGoodsContainer</code> 1-9
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test
    public void test_DangerousGoodsContainer_validCategoryRanges() throws InvalidContainerException {
        for (Integer i = CONT_MIN_CATEGORY; i <= CONT_MAX_CATEGORY; i++) {
            dCont = new DangerousGoodsContainer(code, CONT_WEIGHT, i);
        }
    }

    /**
     * Tests for category values within low ranges to throw exceptions in
     * <code>DangerousGoodsContainer</code>. <br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test
    public void test_DangerousGoodsContainer_invalidCategoryLowRanges()
            throws InvalidContainerException {
        final Integer RANGE = -1000;
        for (Integer i = CONT_MIN_CATEGORY - 1; i >= RANGE; i--) {
            try {
                dCont = new DangerousGoodsContainer(code, CONT_WEIGHT, i);
                fail("Failed at Category: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }

    /**
     * Tests for category values within high ranges to throw exceptions in
     * <code>DangerousGoodsContainer</code>. <br>
     * Test will fail if an exception is <b>not</b> thrown.
     * 
     * @author Thuan Nguyen - N7195508
     * @throws InvalidContainerException
     */
    @Test
    public void test_DangerousGoodsContainer_invalidCategoryHighRanges()
            throws InvalidContainerException {
        final Integer RANGE = 1000;
        for (Integer i = CONT_MAX_CATEGORY + 1; i <= RANGE; i++) {
            try {
                dCont = new DangerousGoodsContainer(code, CONT_WEIGHT, i);
                fail("Failed at Category: " + i.toString());
            } catch (InvalidContainerException e) {
            }
        }
    }
}
