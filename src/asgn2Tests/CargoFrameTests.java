package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.regex.Pattern;

import javax.swing.JFrame;

import org.fest.swing.edt.FailOnThreadViolationRepaintManager;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import asgn2GUI.CargoFrame;
import asgn2GUI.CargoTextFrame;

/**
 * @author Jieun Jang - N8828041
 *
 */
public class CargoFrameTests {
	
    private static final String MAXIMUM_WEIGHT = "Maximum Weight";
    private static final String MAXIMUM_HEIGHT = "Maximum Height";
    private static final String NUMBER_OF_STACKS = "Number of Stacks";
    private static final String START_BARS = "|| ";
    private static final String MID_BARS = " || ";
    private static final String END_BARS = " ||";
    private static final String START_FOUND_BARS = "||*";
    private static final String END_FOUND_BARS = "*||";
    private static final String NEW_LINE = "\n";
    private static final String SPACE = " ";
    private static final String TEMPERATURE2 = "Temperature";
    private static final String GOODS_CATEGORY = "Goods Category";
    private static final String CONTAINER_WEIGHT = "Container Weight";
    private static final String CONTAINER_CODE = "Container Code";
    private static final String CONTAINER_INFORMATION = "Container Information";
    private static final String CARGO_TEXT_AREA = "Cargo Text Area";
    private static final String FIND = "Find";
    private static final String NEW_MANIFEST = "New Manifest";
    private static final String UNLOAD = "Unload";
    private static final String LOAD = "Load";
    private static final String CONTAINER_TYPE = "Container Type";
    private static final String GENERAL_GOODS = "General Goods";
    private static final String REFRIGERATED_GOODS = "Refrigerated Goods";
    private static final String DANGEROUS_GOODS = "Dangerous Goods";
    private static final String CATEGORY_2 = "2";
    private static final String TEMPERATURE_MINUS_4 = "-4";
    private static final String TEMPERATURE_10 = "10";
    private static final String CODE_1 = "ABCU1234564";
    private static final String CODE_2 = "ZZZU6549874";
    private static final String CODE_3 = "JHGU1716760";
    private static final String CODE_4 = "KOCU8090115";
    private static final String CODE_5 = "CSQU3054389";
    private static final String NEGATIVE = "-20";
    private static final String NOT_NUMERIC = "A2A";
    private static final String STACKS_1 = "1";
    private static final String STACKS_2 = "2";
    private static final String STACKS_3 = "3";
    private static final String STACKS_5 = "5";
    private static final String WEIGHT_20 = "20";
    private static final String WEIGHT_30 = "30";
    private static final String WEIGHT_80 = "80";
    private static final String WEIGHT_100 = "100";
    private static final String WEIGHT_200 = "200";
    private static final String HEIGHT_1 = "1";
    private static final String HEIGHT_2 = "2";
    private static final String HEIGHT_5 = "5";
    private static final String NOINPUT = "";
    
    private static final String OVERWEIGHT_CONT = "31";
    private static final String UNDERWEIGHT_CONT = "3";
    private static final String CATEGORY_OVER = "10";
    private static final String CATEGORY_UNDER = "0";
    
    private static final boolean USE_TEXT_VERSION = true;
    
	private static final int NO_PAUSE = 0;
	
    private static final Pattern MANIFEST_EXCEPTION_PATTERN = Pattern.compile(".*ManifestException:.+");
    private static final Pattern INVALID_CONTAINER_PATTERN = Pattern.compile(".*InvalidContainerException:.+");
    private static final Pattern INVALID_CODE_PATTERN = Pattern.compile(".*InvalidCodeException:.+");
    private FrameFixture testFrame;
    private JFrame frameUnderTest;

    /**
     * Turn on automated check to verify all Swing component updates are done in the Swing Event
     * Dispatcher Thread (EDT). The EDT ensures that the application never loses responsiveness to
     * user gestures.
     */
    @BeforeClass
    public static void setUpOnce() {
        FailOnThreadViolationRepaintManager.install();
    }

    /**
     * Ensures that frame is launched through FEST to ensure the EDT is used properly. JUnit runs in
     * its own thread.
     */
    @Before
    public void setUp() {
        if (USE_TEXT_VERSION) {
            /* +----------------------------------------------+ */
            /* | Use a CargoTextFrame to test the application | */
            /* +----------------------------------------------+ */
            frameUnderTest = GuiActionRunner.execute(new GuiQuery<CargoTextFrame>() {
                @Override
                protected CargoTextFrame executeInEDT() {
                    CargoTextFrame cargo = new CargoTextFrame("Cargo Manifest 1.0");
                    return cargo;
                }
            });
        } else {
            /* +------------------------------------------+ */
            /* | Use a CargoFrame to test the application | */
            /* +------------------------------------------+ */
            frameUnderTest = GuiActionRunner.execute(new GuiQuery<CargoFrame>() {
                @Override
                protected CargoFrame executeInEDT() {
                    CargoFrame cargo = new CargoFrame("Cargo Manifest 1.0");
                    return cargo;
                }
            });
        }

        // Choose one of the above JFrames to use as the test fixture.
        testFrame = new FrameFixture(frameUnderTest);
    }

    /**
     * Unload the frame and associated resources.
     */
    @After
    public void tearDown() {
        delay(NO_PAUSE);
        testFrame.cleanUp();
    }

    /**
     * Add a delay so that screen status can be viewed between tests.
     * Select from NO_PAUSE, SHORT_PAUSE, LONG_PAUSE.
     *
     * @param milliseconds The amount of time fow which to pause.
     */
    private void delay(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Ensure the frame used for testing has been instantiated.
     */
    @Test
    public void testConstruction() {
        assertNotNull(testFrame);
    }

    /**
     * Tests that only the "New Manifest" button is enabled when the application starts.
     */
    @Test
    public void buttonStateAtStart() {
        testFrame.button(NEW_MANIFEST).requireEnabled();
        testFrame.button(LOAD).requireDisabled();
        testFrame.button(UNLOAD).requireDisabled();
        testFrame.button(FIND).requireDisabled();
    }

    /*
     * Helper - Brings up a ManifestDilaog for further interaction in tests.
     */
    private DialogFixture prepareManifestDialog() {
        testFrame.button(NEW_MANIFEST).click();
        DialogFixture manifestFixture = testFrame.dialog(NEW_MANIFEST);
        return manifestFixture;
    }

    /*
     * Helper - Puts text in the relevant text areas of the ManifestDialog.
     */
    private void manifestDialogEnterText(DialogFixture dialog, String stacks, String height, String weight) {
        if (stacks != null) {
            dialog.textBox(NUMBER_OF_STACKS).enterText(stacks);
        }
        if (height != null) {
            dialog.textBox(MAXIMUM_HEIGHT).enterText(height);
        }
        if (weight != null) {
            dialog.textBox(MAXIMUM_WEIGHT).enterText(weight);
        }
        dialog.button("OK").click();
    }

    /**
     * Tests for an error message when no text is entered in a ManifestDialog and "OK" is pressed.
     */
    @Test
    public void newManifestNoData() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestFixture.button("OK").click();
        testFrame.optionPane().requireErrorMessage();
    }

    /**
     * Tests for an error message when non-numeric data is entered in "Number of Stacks" text field
     * in a ManifestDialog.
     */
    @Test
    public void newManifestInvalidStacks() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, NOT_NUMERIC, null, null);
        manifestFixture.optionPane().requireErrorMessage();
    }

    /**
     * Tests for an error message when non-numeric data is entered in "Max Stack Height" text
     * field in a ManifestDialog.
     */
    @Test
    public void newManifestValidStacksInvalidHeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, NOT_NUMERIC, null);
        manifestFixture.optionPane().requireErrorMessage();
    }

    /**
     * Tests for an error message when non-numeric data is entered in "Max Weight" text
     * field in a ManifestDialog.
     */
    @Test
    public void newManifestValidStacksValidHeightInvalidWeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, HEIGHT_5, NOT_NUMERIC);
        manifestFixture.optionPane().requireErrorMessage();
    }

    /**
     * Tests for an error message when negative data is entered in "Number of Stacks" text
     * field in a ManifestDialog.
     */
    @Test
    public void newManifestNegativeStacks() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, NEGATIVE, HEIGHT_5, WEIGHT_100);
        manifestFixture.optionPane().requireErrorMessage();
        manifestFixture.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
    }

    /**
     * Tests for an error message when negative data is entered in "Max Stack Height" text
     * field in a ManifestDialog.
     */
    @Test
    public void newManifestNegativeHeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, NEGATIVE, WEIGHT_100);
        manifestFixture.optionPane().requireErrorMessage();
        manifestFixture.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
    }

    /**
     * Tests for an error message when negative data is entered in "Max Weight" text
     * field in a ManifestDialog.
     */
    @Test
    public void newManifestNegativeWeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, HEIGHT_5, NEGATIVE);
        manifestFixture.optionPane().requireErrorMessage();
        manifestFixture.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
    }

    /**
     * Tests for an error message when no data is entered in "Max Height" text
     * field in a ManifestDialog.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void newManifestNoHeightInput() {
        DialogFixture manifestFixture = prepareManifestDialog();        
        manifestDialogEnterText(manifestFixture, STACKS_5, NOINPUT, WEIGHT_100);
        manifestFixture.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message when no data is entered in "Max Weight" text
     * field in a ManifestDialog.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void newManifestNoWeightInput() {
        DialogFixture manifestFixture = prepareManifestDialog();        
        manifestDialogEnterText(manifestFixture, STACKS_5, STACKS_5, NOINPUT);
        manifestFixture.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message when no data is entered in "Number of Stacks" text
     * field in a ManifestDialog.
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void newManifestNoStacksInput() {
        DialogFixture manifestFixture = prepareManifestDialog();        
        manifestDialogEnterText(manifestFixture, NOINPUT, HEIGHT_5, WEIGHT_100);
        manifestFixture.optionPane().requireErrorMessage();
    }    
    
    /**
     * Tests that all buttons are enabled on the window after a valid CargoManifest has been
     * created.
     */
    @Test
    public void newManifestValidDataButtonCheck() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, HEIGHT_5, WEIGHT_100);
        testFrame.button(NEW_MANIFEST).requireEnabled();
        testFrame.button(LOAD).requireEnabled();
        testFrame.button(UNLOAD).requireEnabled();
        testFrame.button(FIND).requireEnabled();
    }

    /**
     * Tests that the Cargo Text Area holds the correct string representation for a single empty
     * stack.
     */
    @Test
    public void newManifestOneEmptyStack() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_100);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that the Cargo Text Area holds the correct string representation for three empty
     * stacks.
     */
    @Test
    public void newManifestThreeEmptyStacks() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_100);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /*
     * Helper - Enters data into a Create Manifest dialog
     */
    private void loadContainer(String containerType, String code, String weight,
            String goodsCat, String temperature) {
        testFrame.button(LOAD).click();
        DialogFixture containerDialog = testFrame.dialog(CONTAINER_INFORMATION);
        containerDialog.comboBox(CONTAINER_TYPE).selectItem(containerType);
        containerDialog.textBox(CONTAINER_CODE).enterText(code);
        containerDialog.textBox(CONTAINER_WEIGHT).enterText(weight);
        if (containerType.equals(DANGEROUS_GOODS)) {
            containerDialog.textBox(GOODS_CATEGORY).enterText(goodsCat);
        } else if (containerType.equals(REFRIGERATED_GOODS)) {
            containerDialog.textBox(TEMPERATURE2).enterText(temperature);
        }
        containerDialog.button("OK").click();
    }

    /**
     * Tests that a valid General Goods container is added and displayed.
     */
    @Test
    public void addGeneralGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that a valid Dangerous Goods container is added and displayed.
     */
    @Test
    public void addDangerousGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_3 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that a valid Refrigerated Container is added and displayed.
     */
    @Test
    public void addRefrigeratedGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_2 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that one valid container of each container type is added to the canvas.
     */
    @Test
    public void addOneOfEach() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests if loadContainer has an error message when no entry is added for weight
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersGGCNoWeight() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, NOINPUT, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests if loadContainer has an error message when no entry is added for container code
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersGGCNoCode() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, NOINPUT, WEIGHT_20, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests if loadContainer has an error message for overweight General Goods Container
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersGGCOverweight() {   
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, OVERWEIGHT_CONT, null, null);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
    }
    
    /**
     * Tests if loadContainer has an error message for underweight General Goods Container
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersGGCUnderweight() {   
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, UNDERWEIGHT_CONT, null, null);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
    }
    
    /**
     * Tests if loadContainer has an error message for invalid owner code
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersInvalidCodeOwner_1() {    
    	final String INVALIDCODE = "dAAU0000000";
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, INVALIDCODE, WEIGHT_20, null, null);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(INVALID_CODE_PATTERN);
    }
    
    /**
     * Tests if loadContainer has an error message for invalid serial code
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersInvalidCodeSerial() {    
    	final String INVALIDCODE = "AAAU00Ac000";
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, INVALIDCODE, WEIGHT_20, null, null);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(INVALID_CODE_PATTERN);
    }
    
    /**
     * Tests if loadContainer has an error message for invalid category code
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersInvalidCodeCategory() {    
    	final String INVALIDCODE = "AAAK0000000";
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, INVALIDCODE, WEIGHT_20, null, null);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(INVALID_CODE_PATTERN);
    }
    
    /**
     * Tests if loadContainer has an error message for invalid category input in 
     * Dangerous Goods
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersDGCCategoryNoInput() {   
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_20, NOINPUT, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests if loadContainer has an error message for invalid category input in 
     * Dangerous Goods
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersDGCCategoryNotNumeric() {   
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_20, NOT_NUMERIC, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests if loadContainer has an error message for overweight Refrigerated Container
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersDGCOverweight() {   
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(DANGEROUS_GOODS, CODE_1, OVERWEIGHT_CONT, CATEGORY_2, null);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
    }
    
    /**
     * Tests if loadContainer has an error message for underweight Refrigerated Container
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersDGCUnderweight() {   
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(DANGEROUS_GOODS, CODE_1, UNDERWEIGHT_CONT, CATEGORY_2, null);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
    }
    
	/**
	 * Tests if loadContainer has an error message for over category range for dangerous
	 * goods container
	 * 
	 * @author Thuan Nguyen - N7195508
	 */
	@Test
	public void loadContainersDGC_categoryOver() {
		manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
		loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_30, CATEGORY_OVER, null);
		testFrame.optionPane().requireErrorMessage();
		testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
	}

	/**
	 * Tests if loadContainer has an error message for under category range for dangerous
	 * goods container
	 * 
	 * @author Thuan Nguyen - N7195508
	 */
	@Test
	public void loadContainersDGC_categoryUnder() {
		manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
		loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_30, CATEGORY_UNDER, null);
		testFrame.optionPane().requireErrorMessage();
		testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
	}

	/**
	 * Tests if loadContainer has an error message for over category range for dangerous
	 * goods container
	 * 
	 * @author Thuan Nguyen - N7195508
	 */
	@Test
	public void loadContainersDGC_categoryOverLarge() {
		final String LARGECAT = "20";
		manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
		loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_30, LARGECAT, null);
		testFrame.optionPane().requireErrorMessage();
		testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
	}

	/**
	 * Tests if loadContainer has an error message for under category range for dangerous
	 * goods container
	 * 
	 * @author Thuan Nguyen - N7195508
	 */
	@Test
	public void loadContainersDGC_categoryUnderLarge() {
		final String LOWCAT = "-20";
		manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
		loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_30, LOWCAT, null);
		testFrame.optionPane().requireErrorMessage();
		testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
	}
	
	/**
	 * Tests if loadContainer has an error message for under category range for dangerous
	 * goods container
	 * 
	 * @author Thuan Nguyen - N7195508
	 */
	@Test
	public void loadContainersDGC_categoryNegative() {
		final String LOWCAT = "-1";
		manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
		loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_30, LOWCAT, null);
		testFrame.optionPane().requireErrorMessage();
		testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
	}
    
    /**
     * Tests if loadContainer has an error message for invalid category input in 
     * Refrigerated Container
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersRCTemperatureNoInput() {   
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(REFRIGERATED_GOODS, CODE_1, WEIGHT_20, null, NOINPUT);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests if loadContainer has an error message for invalid temperature input in 
     * Refrigerated Container
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersRCTemperatureNotNumeric() {   
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(REFRIGERATED_GOODS, CODE_1, WEIGHT_20, null, NOT_NUMERIC);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests if loadContainer has an error message for overweight Refrigerated Container
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersRCOverweight() {   
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(REFRIGERATED_GOODS, CODE_1, OVERWEIGHT_CONT, null, TEMPERATURE_10);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
    }
    
    /**
     * Tests if loadContainer has an error message for underweight Refrigerated Container
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersRCUnderweight() {   
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(REFRIGERATED_GOODS, CODE_1, UNDERWEIGHT_CONT, null, TEMPERATURE_10);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(INVALID_CONTAINER_PATTERN);
    }
    
    /**
     * Tests if loadContainer returns an error message when no entry is added for weight
     * 
     * @author Thuan Nguyen - N7195508
     */
    @Test
    public void loadContainersNoCode() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, NOINPUT, null, null);
    }
    
    /**
     * Tests if loadContainer can load containers on 1 stack
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void loadContainersOneNumstacks() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_4, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_5, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + MID_BARS + CODE_3 + MID_BARS 
            		+ CODE_4 + MID_BARS + CODE_5 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests if loadContainer can load containers for one max height
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void loadContainersOneMaxHeight() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_1, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_20, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_4, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_5, WEIGHT_20, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_2 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + CODE_5 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests if loadContainer can load different containers across the stacks
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void addOneOfEachOnSameStack() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5 , WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null); 
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_2 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    
    /**
     * Tests if loadContainer displays an error message when no more weight can be added
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void addOverGrossWeight() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_5, WEIGHT_80);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_30, null, TEMPERATURE_MINUS_4);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_30, null, null);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests if trying to add a container with the same container code and same type would 
     * display an error
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void addSameContainerCode() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_80);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
    }
    
    /**
     * Tests if adding container with the same code but different type displays an error
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void loadDifferentTypesWithSameCode() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_80);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        loadContainer(REFRIGERATED_GOODS, CODE_1, WEIGHT_30, null, TEMPERATURE_MINUS_4);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
    }

    /**
     * Tests if adding a container when there is no space displays an error
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void overLoadNoSpace() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_2, HEIGHT_2, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_4, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_5, WEIGHT_30, null, null);
        testFrame.optionPane().requireErrorMessage();
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
	/**
	 * Tests if attempting to add a container when stacks are occupied with other
	 * types, or are full would display an error
	 * 
	 * @author Jieun Jang - N8828041
	 */
    @Test
    public void loadContainerWhenStacksFull() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_2, HEIGHT_2 , WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null); 
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_20, null, null); 
        loadContainer(GENERAL_GOODS, CODE_4, WEIGHT_20, null, null);        
        testFrame.optionPane().requireErrorMessage();        
    }

	/**
	 * Tests if adding a different container for 1 occupied stack would display an error
	 * for a max number of stacks of 1
	 * 
	 * @author Jieun Jang
	 */
	@Test
    public void loadContainerDifferentContainerWhenStacksOccupied() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_2 , WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null); 
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        testFrame.optionPane().requireErrorMessage();        
    }

    /**
     * Tests if cargo can successfully add different containers
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void loadContainer_classification() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_2, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_4, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /*
     * Helper - Clicks the Unload button and enters a valid container code.
     */
    private void unloadContainer(String code) {
        testFrame.button(UNLOAD).click();
        DialogFixture containerDialog = testFrame.dialog("Container Dialog");
        containerDialog.textBox(CONTAINER_CODE).enterText(code);
        containerDialog.button("OK").click();
    }
    
    /**
     * Tests if attempting to unload an empty manifest displays an error
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void unloadEmptyManifest() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_80);
        unloadContainer(CODE_1);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
    }
    
    /**
     * Tests if unloading a General Goods Container can be performed without errors
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void unloadGeneralGoodsContainer() {
    	 manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
    	 loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
    	 if (frameUnderTest instanceof CargoTextFrame) {
             String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE;
             assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
         }
    	 
    	 unloadContainer(CODE_1);
    	 if (frameUnderTest instanceof CargoTextFrame) {
             String expected = START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE;
             assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
         }
    }
    
    /**
     * Tests if unloading a Dangerous Goods Container can be performed without errors
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void unloadDangerousGoodsContainer() {
    	 manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
    	 loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_30, CATEGORY_2, null);
    	 if (frameUnderTest instanceof CargoTextFrame) {
             String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE;
             assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
         }
    	 
    	 unloadContainer(CODE_1);
    	 if (frameUnderTest instanceof CargoTextFrame) {
             String expected = START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE;
             assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
         }
    }
    
    /**
     * Tests if unloading a General Goods Container can be performed without errors
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void unloadRefrigeratedContainer() {
    	 manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
    	 loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_30, CATEGORY_2, null);
    	 if (frameUnderTest instanceof CargoTextFrame) {
             String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE;
             assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
         }
    	 
    	 unloadContainer(CODE_1);
    	 if (frameUnderTest instanceof CargoTextFrame) {
             String expected = START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE
                     + START_BARS + END_BARS + NEW_LINE;
             assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
         }
    }
    
    /**
     * Tests if unloading a Refrigerated Container can be performed without errors
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void unloadFromEmptyManifest() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        unloadContainer(CODE_1);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
	/**
	 * Tests if attemtping to unload a container that is currently not onboard would
	 * display an error
	 * 
	 * @author Jieun Jang - N8828041
	 */
    @Test
    public void unloadNotOnBoard() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_30, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_30, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_4);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
	 * Tests if attempting to unload a container that has already been unloaded would
	 * display an error
	 * 
	 * @author Jieun Jang - N8828041
	 */
    @Test
    public void unloadContainerAlreadyUnloaded() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_30, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_2);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_2);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
    }

    /**
	 * Tests if the container below an unloaded container can be unloaded
	 * 
	 * @author Jieun Jang - N8828041
	 */
    @Test
    public void unload2ndAddedContainerAfterAddOneOfEach() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_30, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_30, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_2);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
	 * Tests if the container below an unloaded container can be unloaded
	 * 
	 * @author Jieun Jang - N8828041
	 */
    @Test
    public void unloadFirstContainerAdded() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_5, WEIGHT_80);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_30, null, null);
        
        unloadContainer(CODE_1);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
	/**
	 * Tests if the attempting to unload a container that's underneath other containers
	 * would display an error
	 * 
	 * @author Jieun Jang - N8828041
	 */
    @Test
    public void unloadContainerBetweenTwo() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_30, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + MID_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        
        unloadContainer(CODE_2);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + MID_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    
    /**
	 * Tests if the container below the unloaded container can be unloaded
	 * 
	 * @author Jieun Jang - N8828041
	 */
    @Test
    public void unloadContainerFirstAddedAfterAddOtherTypes() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_30, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_3, WEIGHT_30, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_4, WEIGHT_30, CATEGORY_2, null);
        
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_1);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
	 * Tests for an irregular load and unload with Dangerous Goods Container
	 * 
	 * @author Jieun Jang - N8828041
	 */
    @Test
    public void unloadFirstDangerouseContainer() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_30, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_3, WEIGHT_30, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_4, WEIGHT_30, CATEGORY_2, null);
        
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_4);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
	 * Tests for an irregular load and unload with Dangerous Goods Container
	 * 
	 * @author Jieun Jang - N8828041
	 */
    @Test
    public void unloadFirstDangerouseContainerAfterAddOneMore() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_30, CATEGORY_2, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_30, null, null);
        loadContainer(DANGEROUS_GOODS, CODE_4, WEIGHT_30, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_3);
        testFrame.optionPane().requireErrorMessage();
        testFrame.optionPane().requireMessage(MANIFEST_EXCEPTION_PATTERN);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
	 * Tests if all containers can be unloaded given the correct conditions without errors
	 * 
	 * @author Jieun Jang - N8828041
	 */
    @Test
    public void unloadAllContainers() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_30, null, null);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_30, CATEGORY_2, null);
        loadContainer(DANGEROUS_GOODS, CODE_4, WEIGHT_30, CATEGORY_2, null);
        loadContainer(REFRIGERATED_GOODS, CODE_5, WEIGHT_30, null, TEMPERATURE_MINUS_4);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + CODE_5 + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_2);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + CODE_5 + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_4);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + CODE_5 + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_5);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_3);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_1);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that a loaded container can then be unloaded.
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void loadGeneralGoodsThenUnload() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_1);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /*
     * Helper - Clicks the Find button and enters a valid container code.
     */
    private void findContainer(String code) {
        testFrame.button(FIND).click();
        DialogFixture containerDialog = testFrame.dialog("Container Dialog");
        containerDialog.textBox(CONTAINER_CODE).enterText(code);
        containerDialog.button("OK").click();
    }
    
    /**
     * Tests if 1 container can be found
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void findContainer() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }        
        findContainer(CODE_1);
        
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_FOUND_BARS + CODE_1 + END_FOUND_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests if a container between other containers can be found
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void findGeneralContainerOneStack() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_4, WEIGHT_30, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + MID_BARS + CODE_3 
            		+ MID_BARS + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }        
        findContainer(CODE_3);
        
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + SPACE + START_FOUND_BARS 
            		+ CODE_3 + END_FOUND_BARS + SPACE + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    
    /**
     * Tests if a Refrigerated Container can be found
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void findRefrigeratedContainerOneStack() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_5, WEIGHT_200);
        loadContainer(REFRIGERATED_GOODS, CODE_1, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(REFRIGERATED_GOODS, CODE_3, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(REFRIGERATED_GOODS, CODE_4, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + MID_BARS + CODE_3 
            		+ MID_BARS + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }        
        findContainer(CODE_3);
        
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + SPACE + START_FOUND_BARS 
            		+ CODE_3 + END_FOUND_BARS + SPACE + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests if a Dangerous Goods Container can be found
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void findDangerousContainerOneStack() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_5, WEIGHT_200);
        loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_30, CATEGORY_2, null);
        loadContainer(DANGEROUS_GOODS, CODE_2, WEIGHT_30, CATEGORY_2, null);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_30, CATEGORY_2, null);
        loadContainer(DANGEROUS_GOODS, CODE_4, WEIGHT_30, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + MID_BARS 
            		+ CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }        
        findContainer(CODE_3);
        
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + SPACE + START_FOUND_BARS
            		+ CODE_3 + END_FOUND_BARS + SPACE + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests if a container can be found within multiple stacks
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void findContainerMultipleStacks() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_30, CATEGORY_2, null);
        loadContainer(DANGEROUS_GOODS, CODE_4, WEIGHT_30, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_2 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }        
        findContainer(CODE_4);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_2 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + SPACE + START_FOUND_BARS + CODE_4 + END_FOUND_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests if a different container can be found after a find as been done with one container
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void find2ContainerAtTheSameTime() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_30, CATEGORY_2, null);
        loadContainer(DANGEROUS_GOODS, CODE_4, WEIGHT_30, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_2 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }        
        findContainer(CODE_4);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS +  CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_2 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + SPACE + START_FOUND_BARS + CODE_4 + END_FOUND_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        findContainer(CODE_1);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_FOUND_BARS +  CODE_1 + END_FOUND_BARS + NEW_LINE
            		+ START_BARS + CODE_2 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    
    /**
     * Tests if trying to find an already unloaded container woud throw an error 
     * 
     * @author Jieun Jang - N8828041
     */
    @Test
    public void loadAndUnloadAndFindContainer() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_5, HEIGHT_5, WEIGHT_200);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
        loadContainer(GENERAL_GOODS, CODE_2, WEIGHT_30, null, null);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_30, CATEGORY_2, null);
        loadContainer(DANGEROUS_GOODS, CODE_4, WEIGHT_30, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + MID_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_2);
        
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + MID_BARS + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        
        findContainer(CODE_3);
        
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_FOUND_BARS + CODE_3 + END_FOUND_BARS + SPACE + CODE_4 + END_BARS + NEW_LINE
                    + START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        
        findContainer(CODE_2);
        testFrame.optionPane().requireErrorMessage();
    }
    
	/**
	 * Tests for the graphical section where a scrollbar appears when items are pushed of
	 * the canvas space
	 * 
	 * @author Thuan Nguyen - N7195508
	 */
	@Test
    public void multiStack_multiHeight() {
    	if (frameUnderTest instanceof CargoFrame) {
	    	final String STACKS = "6";
	    	final String HEIGHT = "10";
	    	final String WEIGHT = "200";
	        manifestDialogEnterText(prepareManifestDialog(), STACKS, HEIGHT, WEIGHT);
	        loadContainer(GENERAL_GOODS, CODE_1, "4", null, null);
	        loadContainer(DANGEROUS_GOODS, "AAAU0000000", "4", "1", null);
	        loadContainer(DANGEROUS_GOODS, "AAAU0000011", "4", "1", null);
	        loadContainer(REFRIGERATED_GOODS, "AAAU0000022", "4", null, "1");
	        loadContainer(DANGEROUS_GOODS, "AAAU0000033", "4", "1", null);   
	        loadContainer(REFRIGERATED_GOODS, "AAAU0000044", "4", null, "1");
	        loadContainer(GENERAL_GOODS, "AAAU0000055", "4", null, null);
	        loadContainer(GENERAL_GOODS, "AAAU0000066", "4", null, null);
	        loadContainer(GENERAL_GOODS, "AAAU0000077", "4", null, null);
	        loadContainer(GENERAL_GOODS, "AAAU0000088", "4", null, null);
	        loadContainer(GENERAL_GOODS, "AAAU0000099", "4", null, null);
    	}
    }
    
	/**
	 * Tests for the graphical section where a scrollbar appears when items are pushed of
	 * the canvas space for 1 stack
	 * 
	 * @author Thuan Nguyen - N7195508
	 */
    @Test
    public void checkSrollable_vertical() {
		if (frameUnderTest instanceof CargoFrame) {
			final String STACKS = "10";
			final String HEIGHT = "10";
			final String WEIGHT = "200";
			manifestDialogEnterText(prepareManifestDialog(), STACKS, HEIGHT, WEIGHT);
			loadContainer(GENERAL_GOODS, CODE_1, "4", null, null);
			loadContainer(GENERAL_GOODS, "AAAU0000055", "4", null, null);
			loadContainer(GENERAL_GOODS, "AAAU0000066", "4", null, null);
			loadContainer(GENERAL_GOODS, "AAAU0000077", "4", null, null);
			loadContainer(GENERAL_GOODS, "AAAU0000088", "4", null, null);
			loadContainer(GENERAL_GOODS, "AAAU0000099", "4", null, null);
			loadContainer(GENERAL_GOODS, "AAAU0000101", "4", null, null);
			loadContainer(GENERAL_GOODS, "AAAU0000112", "4", null, null);
		}
    }

}
