package asgn2Tests;

/* Some valid container codes used in the tests below:
 * INKU2633836
 * KOCU8090115
 * MSCU6639871
 * CSQU3054389
 * QUTU7200318
 * IBMU4882351
 */

import org.junit.Before;
import org.junit.Test;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.FreightContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.InvalidCodeException;
import asgn2Exceptions.InvalidContainerException;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;
import static org.junit.Assert.*;

/**
 * @author Jieun Jang - N8828041
 *
 */
public class ManifestTests {
	
    CargoManifest manifest;
	FreightContainer gContainer;
	FreightContainer dContainer;
	FreightContainer rContainer;
	ContainerCode code;
	ContainerCode gCode;
	ContainerCode dCode;
	ContainerCode rCode;

	// Default constants and variables used for testing various 
	// CargoManifest
	private static final Integer NEGATIVE = -5;
	private static final Integer ZERO = 0;
	
	private static final Integer NUMSTACKS_1 = 1;
	private static final Integer NUMSTACKS_2 = 2;
	private static final Integer NUMSTACKS_3 = 3;
	
	private static final Integer MAXHEIGHT_1 = 1;
	private static final Integer MAXHEIGHT_2 = 2;
    private static final Integer MAXHEIGHT_3 = 3;
    
	private static final Integer MAXWEIGHT = 200;
	private static final Integer MAXWEIGHT_100 = 100;

	private static final Integer DEFAULT_CATEGORY = 2;
	private static final Integer DEFAULT_TEMPERATURE = 20;

	private static final Integer GROSSWEIGHT_30 = 30;
	private static final Integer GROSSWEIGHT_20 = 20;
	private static final Integer GROSSWEIGHT_10 = 10;
	
	private static final String CODE1 = "INKU2633836";
	private static final String CODE2 = "KOCU8090115";
	private static final String CODE3 = "MSCU6639871";
	private static final String CODE4 = "CSQU3054389";
	private static final String CODE5 = "IBMU4882351";
	private static final String CODE6 = "AAAU0000055";
	
	private static final String START_BARS = "|| ";
    private static final String MID_BARS = " || ";
    private static final String END_BARS = " ||";
    private static final String START_FOUND_BARS = "||*";
    private static final String END_FOUND_BARS = "*||";
    private static final String NEW_LINE = "\n";
    private static final String SPACE = " ";

	///// Test for CargoManifest ////
	/**
	 * Tests the constructor for CargoManifest 
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 */
	@Before
	@Test
	public void test_CargoManifest() throws ManifestException {
		manifest = new CargoManifest(NUMSTACKS_3, MAXHEIGHT_3, MAXWEIGHT);
	}

	/**
	 * Tests if CargoManifest can successfully construct an object with all 0 parameters
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 */
	@Test
	public void test_CargoManifest_allZero() throws ManifestException {
		manifest = new CargoManifest(ZERO, ZERO, ZERO);
	}
	
	/**
	 * Tests if CargoManifest throws an exception for a negative stack
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void test_CargoManifest_negativeNumStacks() throws ManifestException {
		manifest = new CargoManifest(NEGATIVE, MAXHEIGHT_3, MAXWEIGHT);
	}
	
	/**
	 * Tests if CargoManifest throws an exception when a stack is negative while height
	 * and weight are zero.
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void test_CargoManifest_negativeStacksOthersZero() throws ManifestException {
		manifest = new CargoManifest(NEGATIVE, ZERO, ZERO);
	}

	/**
	 * Tests if CargoManifest throws an exception for negative max height
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void test_CargoManifest_negativeHeight() throws ManifestException {
		manifest = new CargoManifest(NUMSTACKS_3, NEGATIVE, MAXWEIGHT);
	}
	
	/**
	 * Tests if CargoManifest throws an exception for negative max height when stacks and
	 * weight are zero
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void test_CargoManifest_negativeHeightOthersZero() throws ManifestException {
		manifest = new CargoManifest(ZERO, NEGATIVE, ZERO);
	}

	/**
	 * Tests if CargoManifest throws an exception for negative max weight
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void test_CargoManifest_negativeWeight() throws ManifestException {
		manifest = new CargoManifest(NUMSTACKS_3, MAXHEIGHT_3, NEGATIVE);
	}
	
	/**
	 * Tests if CargoManifest throws an exception for negative max weight when stacks and
	 * height are zero
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void test_CargoManifest_negativeWeightOthersZero() throws ManifestException {
		manifest = new CargoManifest(ZERO, ZERO, NEGATIVE);
	}
	
	/**
	 * Tests if CargoManifest throws an exception for negative max height when stacks and
	 * weight are zero
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void test_CargoManifest_allNegative() throws ManifestException {
		manifest = new CargoManifest(NEGATIVE, NEGATIVE, NEGATIVE);
	}
	
	/**
	 * Tests if a GeneralGoodsContainer can be loaded
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_loadContainer_generalContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
	}
	
	/**
	 * Tests if a RefrigeratedContainer can be loaded
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_loadRefrigeratedContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		rCode = new ContainerCode(CODE1);
		rContainer = new RefrigeratedContainer(rCode, GROSSWEIGHT_30, DEFAULT_TEMPERATURE);
		manifest.loadContainer(rContainer);
	}
	
	/**
	 * Tests if a DangerousGoodsContainer can be loaded
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_loadDangerousContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		dCode = new ContainerCode(CODE1);
		dContainer = new DangerousGoodsContainer(dCode, GROSSWEIGHT_30, DEFAULT_CATEGORY);
		manifest.loadContainer(dContainer);
	}

	/**
	 * Test load container till reach maxheight with numStacks value 1
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_loadContainer_oneStack_Maxheight() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_1, MAXHEIGHT_3, MAXWEIGHT);

		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_10);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE2);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE3);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);
	}
	
	/**
	 * Test load one of each type of container
	 *  
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_loadContainer_oneOfEach()
			throws ManifestException, InvalidCodeException,	InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_3, MAXHEIGHT_3, MAXWEIGHT);

		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_10);
		manifest.loadContainer(gContainer);

		rCode = new ContainerCode(CODE2);
		rContainer = new RefrigeratedContainer(rCode, GROSSWEIGHT_20, DEFAULT_TEMPERATURE);
		manifest.loadContainer(rContainer);

		dCode = new ContainerCode(CODE3);
		dContainer = new DangerousGoodsContainer(dCode, GROSSWEIGHT_20, DEFAULT_CATEGORY);
		manifest.loadContainer(dContainer);
	}
	
	/**
	 * Test that if a different type of container can be loaded on 1 stack
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void test_loadDifferentContainerOneStack() throws ManifestException,
	        InvalidCodeException, InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_1, MAXHEIGHT_3, MAXWEIGHT_100);
		
		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		
		dCode = new ContainerCode(CODE2);
		dContainer = new DangerousGoodsContainer(dCode, GROSSWEIGHT_30, DEFAULT_CATEGORY);
		manifest.loadContainer(dContainer);
		
		rCode = new ContainerCode(CODE3);
		rContainer = new RefrigeratedContainer(rCode, GROSSWEIGHT_30, DEFAULT_TEMPERATURE);
		manifest.loadContainer(rContainer);
	}

	/**
	 * Test if an exception is thrown when attempting to add a container with a code that
	 * is already existing in the cargo.
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void test_loadContainer_sameCode() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
	}
	
	/**
	 * Test if an exception is thrown when attempting to add a container with a code that
	 * is already existing in the cargo but a different type.
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void test_loadContainer_sameCode_differentType() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		dCode = new ContainerCode(CODE1);
		dContainer = new DangerousGoodsContainer(dCode, GROSSWEIGHT_30, DEFAULT_CATEGORY);
		manifest.loadContainer(dContainer);
	}

	/**
	 * Test that cannot add container when over max weight 
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void test_loadContainer_overMaxWeight_multiStack() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_2, MAXHEIGHT_2, MAXWEIGHT_100);

		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE2);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE3);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE4);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
	}

	/**
	 * Tests for exceptions for overweight limits with 1 stack
	 * 
	 * @author Thuan Nguyen - N7195508
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
    public void test_loadContainer_overGrossWeight_singleStack() throws ManifestException,
            InvalidCodeException, InvalidContainerException {
        manifest = new CargoManifest(NUMSTACKS_1, MAXHEIGHT_3 + 1, MAXWEIGHT_100);

        gCode = new ContainerCode(CODE1);
        gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
        manifest.loadContainer(gContainer);

        gCode = new ContainerCode(CODE2);
        gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
        manifest.loadContainer(gContainer);

        gCode = new ContainerCode(CODE3);
        gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
        manifest.loadContainer(gContainer);

        gCode = new ContainerCode(CODE4);
        gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
        manifest.loadContainer(gContainer);
    }
	
	/**
	 * Tests for exceptions in overweight limits with 1 stack height across 4 stacks.
	 * 
	 * @author Thuan Nguyen - N7195508
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void test_loadContainer_overGrossWeight_oneHeight() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_3 + 1, MAXHEIGHT_1, MAXWEIGHT_100);

		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE2);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE3);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE4);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
	}

	/**
	 * Test that cannot add container when manifest is full even it is not over
	 * the max weight
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void test_loadContainer_manifestFull() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_2, MAXHEIGHT_2, MAXWEIGHT);

		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_10);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE2);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE3);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE4);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);

		gCode = new ContainerCode(CODE5);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);
	}
	
	/**
	 * Test that cannot add container when stacks are full
	 * and cannot added in the stack if there are another type of containers
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void test_loadContainer_stacksFull() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_2, MAXHEIGHT_2, MAXWEIGHT);

		// stack 1: GeneralGoodsContaienr
		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_10);
		manifest.loadContainer(gContainer);
		
		// stack 2: DangerousGoddsContainer
		dCode = new ContainerCode(CODE2);
		dContainer = new DangerousGoodsContainer(dCode, GROSSWEIGHT_20, DEFAULT_CATEGORY);
		manifest.loadContainer(dContainer);
		
		// stack 1 
		gCode = new ContainerCode(CODE3);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);

		// exception: stack 1 is full
		gCode = new ContainerCode(CODE5);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);
	}
	
	/**
	 * Container can added in the stack unless there are another type of containers
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_loadContainer_loadOnNext() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_3, MAXHEIGHT_2, MAXWEIGHT);

		// stack 1
		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_10);
		manifest.loadContainer(gContainer);
		
		// stack 2
		dCode = new ContainerCode(CODE2);
		dContainer = new DangerousGoodsContainer(dCode, GROSSWEIGHT_20, DEFAULT_CATEGORY);
		manifest.loadContainer(dContainer);
		
		// stack 1 
		gCode = new ContainerCode(CODE3);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);

		// stack 3
		gCode = new ContainerCode(CODE5);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);
	}

	/**
	 * Tests if a GeneralGoodsContainer can be unloaded
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_unloadGeneralGoodsContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_20);
		manifest.loadContainer(gContainer);
		manifest.unloadContainer(gCode);
	}
	
	/**
	 * Tests if a DangerousGoodsContainer can be unloaded
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_unloadDangerousGoodsContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		dCode= new ContainerCode(CODE1);
		dContainer = new DangerousGoodsContainer(dCode, GROSSWEIGHT_20, DEFAULT_CATEGORY);
		manifest.loadContainer(dContainer);
		manifest.unloadContainer(dCode);
	}
	
	/**
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_unloadRefrigeratedContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {		
		rCode = new ContainerCode(CODE1);
		rContainer = new RefrigeratedContainer(rCode, GROSSWEIGHT_20, DEFAULT_TEMPERATURE);
		manifest.loadContainer(rContainer);
		manifest.unloadContainer(rCode);
	}
	
	/**
	 * Throw exception when unload container from empty manifest
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void test_unloadContainerFromEmpty() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		code = new ContainerCode(CODE4);
		manifest.unloadContainer(code);
	}
	
	/**
	 * Throw exception when unload container not exist on board
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void test_unloadContainer_notOnBoard() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		ContainerCode gCode1 = new ContainerCode(CODE1);
		FreightContainer gContainer1 = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);
		ContainerCode gCode2 = new ContainerCode(CODE2);
		FreightContainer gContainer2 = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);
		ContainerCode gCode3 = new ContainerCode(CODE4);

		manifest.loadContainer(gContainer1);
		manifest.loadContainer(gContainer2);
		manifest.unloadContainer(gCode3);
	}
	
	/**
	 * Unload a container and then try to unload the container again
	 * Throw exception when unload container already unloaded
	 * Can check the container deleted from the array list properly
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void unloadContainerAlreadyUnloaded() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		ContainerCode gCode1 = new ContainerCode(CODE1);
		FreightContainer gContainer1 = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);

		ContainerCode gCode2 = new ContainerCode(CODE2);
		FreightContainer gContainer2 = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);

		manifest.loadContainer(gContainer1);
		manifest.loadContainer(gContainer2);
		manifest.unloadContainer(gCode1);
		manifest.unloadContainer(gCode1);
	}
	
	/**
	 * Unload all containers on the board in order and  test reverse order
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void unloadAllAddedContainers() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		ContainerCode gCode1 = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);

		ContainerCode dCode1 = new ContainerCode(CODE2);
		dContainer = new DangerousGoodsContainer(dCode1, GROSSWEIGHT_30, DEFAULT_CATEGORY);

		ContainerCode rCode1 = new ContainerCode(CODE3);
		rContainer = new RefrigeratedContainer(rCode1, GROSSWEIGHT_30, DEFAULT_TEMPERATURE);

		//remove in order
		manifest.loadContainer(gContainer);
		manifest.loadContainer(dContainer);
		manifest.loadContainer(rContainer);		
		manifest.unloadContainer(gCode1);
		manifest.unloadContainer(dCode1);
		manifest.unloadContainer(rCode1);
		
		//test reverse order
        manifest.loadContainer(rContainer);
        manifest.loadContainer(dContainer);
		manifest.loadContainer(gContainer);
        manifest.unloadContainer(gCode1);
        manifest.unloadContainer(dCode1);
        manifest.unloadContainer(rCode1);
	}
	
	
    /**
     * Tests if load and unload within minimum weights and limits can be performed in 
     * different orders
     * 
     * @author Thuan Nguyen - N7195508
     * @throws ManifestException
     * @throws InvalidCodeException
     * @throws InvalidContainerException
     */
    @Test
    public void loadUnloadMultiStacks_minWeights() throws ManifestException,
            InvalidCodeException, InvalidContainerException {
        final Integer MAXWEIGHT = 24;
        final Integer CONTWEIGHT = 4;
        manifest = new CargoManifest(NUMSTACKS_3, MAXHEIGHT_2, MAXWEIGHT);
        
        ContainerCode gCode1 = new ContainerCode(CODE1);
        gContainer = new GeneralGoodsContainer(gCode1, CONTWEIGHT);        
        ContainerCode gCode2 = new ContainerCode("AAAU0000000");
        FreightContainer gContainer2 = new GeneralGoodsContainer(gCode2, CONTWEIGHT);

        ContainerCode dCode1 = new ContainerCode(CODE2);
        dContainer = new DangerousGoodsContainer(dCode1, CONTWEIGHT, DEFAULT_CATEGORY);
        ContainerCode dCode2 = new ContainerCode("AAAU0000011");
        FreightContainer dContainer2 = new DangerousGoodsContainer(dCode2, CONTWEIGHT,
                DEFAULT_CATEGORY);
        
        ContainerCode rCode1 = new ContainerCode(CODE3);
        rContainer = new RefrigeratedContainer(rCode1, CONTWEIGHT, DEFAULT_TEMPERATURE);
        ContainerCode rCode2 = new ContainerCode("AAAU0000022");
        FreightContainer rContainer2 = new RefrigeratedContainer(rCode2, CONTWEIGHT, 
                DEFAULT_TEMPERATURE);
        
        //remove by stack/column
        manifest.loadContainer(gContainer);
        manifest.loadContainer(dContainer);
        manifest.loadContainer(rContainer);  
        manifest.loadContainer(rContainer2);
        manifest.loadContainer(dContainer2);
        manifest.loadContainer(gContainer2);
        
        manifest.unloadContainer(gCode2);
        manifest.unloadContainer(gCode1);
        manifest.unloadContainer(dCode2);
        manifest.unloadContainer(dCode1);
        manifest.unloadContainer(rCode2);
        manifest.unloadContainer(rCode1);
        
        //remove by row
        manifest.loadContainer(gContainer);
        manifest.loadContainer(dContainer);
        manifest.loadContainer(rContainer);  
        manifest.loadContainer(rContainer2);
        manifest.loadContainer(dContainer2);
        manifest.loadContainer(gContainer2);
        
        manifest.unloadContainer(gCode2);
        manifest.unloadContainer(dCode2);
        manifest.unloadContainer(rCode2);
        manifest.unloadContainer(rCode1);
        manifest.unloadContainer(dCode1);
        manifest.unloadContainer(gCode1);
    }
    
	/**
	 * Test that cannot unload container on the bottom
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void unloadFirstContainer_onTheBottom() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		ContainerCode gCode1 = new ContainerCode(CODE1);
		FreightContainer gContainer1 = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);

		ContainerCode gCode2 = new ContainerCode(CODE2);
		FreightContainer gContainer2 = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);

		manifest.loadContainer(gContainer1);
		manifest.loadContainer(gContainer2);
		
		manifest.unloadContainer(gCode1);
	}
	
	/**
	 * Test that cannot unload container if it is placed between two containers
	 * or there is another container on top
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void unloadContainerBetweenTwo() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		ContainerCode gCode1 = new ContainerCode(CODE1);
		FreightContainer gContainer1 = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);

		ContainerCode gCode2 = new ContainerCode(CODE3);
		FreightContainer gContainer2 = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);

		ContainerCode gCode3 = new ContainerCode(CODE2);
		FreightContainer gContainer3 = new GeneralGoodsContainer(gCode3, GROSSWEIGHT_30);

		manifest.loadContainer(gContainer1);
		manifest.loadContainer(gContainer2);
		manifest.loadContainer(gContainer3);
		
		manifest.unloadContainer(gCode2);
	}
	
	/**
	 * Test that cannot unload container added first, when there are other containers
	 * on the top and next to it
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void unloadContainer_afterAddMultipleType() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		ContainerCode gCode1 = new ContainerCode(CODE1);
		FreightContainer gContainer1 = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);

		ContainerCode gCode2 = new ContainerCode(CODE2);
		FreightContainer gContainer2 = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);

		ContainerCode gCode3 = new ContainerCode(CODE3);
		dContainer = new DangerousGoodsContainer(gCode3, GROSSWEIGHT_30, DEFAULT_CATEGORY);

		ContainerCode gCode4 = new ContainerCode(CODE4);
		rContainer = new RefrigeratedContainer(gCode4, GROSSWEIGHT_30, DEFAULT_TEMPERATURE);

		manifest.loadContainer(gContainer1);
		manifest.loadContainer(dContainer);
		manifest.loadContainer(rContainer);
		manifest.loadContainer(gContainer2);
		
		manifest.unloadContainer(gCode1);
	}
	
	/**
	 * Test that same goods type containers are placed in same stack
	 * and whichStack method
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_whichStack_afterloadTwoSameGoods() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		ContainerCode gCode1 = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		ContainerCode gCode2 = new ContainerCode(CODE2);
		gContainer = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		final Integer EXPECTED = 0;
		assertEquals(EXPECTED, manifest.whichStack(gCode1));
		assertEquals(EXPECTED, manifest.whichStack(gCode2));
	}
	
	/**
	 * Test that add one of each and each one is placed in different stacks
	 * and whichStack method
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_whichStack_oneEachType() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		ContainerCode gCode1 = new ContainerCode(CODE1);
		FreightContainer gContainer = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);

		ContainerCode dCode1 = new ContainerCode(CODE2);
		FreightContainer dContainer = new DangerousGoodsContainer(dCode1,
		        GROSSWEIGHT_30, DEFAULT_CATEGORY);

		ContainerCode rCode1 = new ContainerCode(CODE4);
		FreightContainer rContainer = new RefrigeratedContainer(rCode1,
		        GROSSWEIGHT_30, DEFAULT_TEMPERATURE);

		manifest.loadContainer(gContainer);
		manifest.loadContainer(dContainer);
		manifest.loadContainer(rContainer);
		
		final Integer EXPECTED_0 = 0;
		final Integer EXPECTED_1 = 1;
		final Integer EXPECTED_2 = 2;
		assertEquals(EXPECTED_0, manifest.whichStack(gCode1));
        assertEquals(EXPECTED_1, manifest.whichStack(dCode1));
        assertEquals(EXPECTED_2, manifest.whichStack(rCode1));
	}
	
	/**
	 * Test that same goods type containers are placed in same stack.
	 * If the stack is full, it is placed in another stack as close to the bridge
	 * as possible
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_whichStack_loadSameGoodsContainerOnStackNum2() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_2, MAXHEIGHT_2, MAXWEIGHT);

		ContainerCode gCode1 = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		ContainerCode gCode2 = new ContainerCode(CODE2);
		gContainer = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		ContainerCode gCode3 = new ContainerCode(CODE4);
		gContainer = new GeneralGoodsContainer(gCode3, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		ContainerCode gCode4 = new ContainerCode(CODE5);
		gContainer = new GeneralGoodsContainer(gCode4, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		final Integer EXPECTED_0 = 0;
        final Integer EXPECTED_1 = 1;
		assertEquals(EXPECTED_0, manifest.whichStack(gCode1));
        assertEquals(EXPECTED_0, manifest.whichStack(gCode2));
        assertEquals(EXPECTED_1, manifest.whichStack(gCode3));
        assertEquals(EXPECTED_1, manifest.whichStack(gCode4));
	}
	
	/**
	 * Test that containers are placed according to their type, different goods types
	 * are placed in different stacks
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_whichStack_sameGoodsOnDifferentStacks() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
	    manifest = new CargoManifest(NUMSTACKS_3 + 1, MAXHEIGHT_2, MAXWEIGHT);

		ContainerCode gCode1 = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);
		
        ContainerCode gCode2 = new ContainerCode(CODE5);
        FreightContainer gContainer2 = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);
        
        ContainerCode gCode3 = new ContainerCode("AAAU0000055");
        FreightContainer gContainer3 = new GeneralGoodsContainer(gCode3, GROSSWEIGHT_30);
        
        ContainerCode dCode1 = new ContainerCode(CODE2);
        dContainer = new DangerousGoodsContainer(dCode1, GROSSWEIGHT_30, DEFAULT_CATEGORY);
        
        ContainerCode rCode1 = new ContainerCode(CODE4);
        rContainer = new RefrigeratedContainer(rCode1, GROSSWEIGHT_30, DEFAULT_TEMPERATURE);
        
		manifest.loadContainer(gContainer);		
		manifest.loadContainer(dContainer);		
		manifest.loadContainer(rContainer);
		manifest.loadContainer(gContainer2);
		manifest.loadContainer(gContainer3);		
		
		final Integer EXPECTED_0 = 0;
		final Integer EXPECTED_1 = 1;
		final Integer EXPECTED_2 = 2;
		final Integer EXPECTED_3 = 3;
		
		assertEquals(EXPECTED_0, manifest.whichStack(gCode1));
		assertEquals(EXPECTED_0, manifest.whichStack(gCode2));
		assertEquals(EXPECTED_1, manifest.whichStack(dCode1));
		assertEquals(EXPECTED_2, manifest.whichStack(rCode1));		
		assertEquals(EXPECTED_3, manifest.whichStack(gCode3));
	}

	/**
	 * Tests if howHigh returns 0 for 1 container.
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_howHigh_addOneContainer() throws ManifestException, InvalidCodeException,
			InvalidContainerException {

		gCode = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode, GROSSWEIGHT_30);
		
		manifest.loadContainer(gContainer);

		final Integer EXPECTED_0 = 0;
		
		assertEquals(EXPECTED_0, manifest.howHigh(gCode));
	}
	
	/**
	 * Tests if howHigh returns the correct values for 3 containers stacked on top of each other 
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_howHigh_oneStack() throws ManifestException, InvalidCodeException,
			InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_1, MAXHEIGHT_3, MAXWEIGHT);
		
		ContainerCode gCode1 = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		
		ContainerCode gCode2 = new ContainerCode(CODE2);
		gContainer = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);	
		manifest.loadContainer(gContainer);
		
		ContainerCode gCode3 = new ContainerCode(CODE4);
		gContainer = new GeneralGoodsContainer(gCode3, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		final Integer EXPECTED_0 = 0;
		final Integer EXPECTED_1 = 1;
		final Integer EXPECTED_2 = 2;
		
		assertEquals(EXPECTED_0, manifest.howHigh(gCode1));
		assertEquals(EXPECTED_1, manifest.howHigh(gCode2));
		assertEquals(EXPECTED_2, manifest.howHigh(gCode3));
	}
	
	/**
	 * Tests if howHigh detects the correct placements for irregular loads
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_howHigh_addOneEachAndCheckHeight() throws ManifestException, 
	        InvalidCodeException, InvalidContainerException {
		ContainerCode gCode1 = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		
		ContainerCode dCode = new ContainerCode(CODE2);
		dContainer = new DangerousGoodsContainer(dCode, GROSSWEIGHT_30, DEFAULT_CATEGORY);
		manifest.loadContainer(dContainer);
		
		ContainerCode rCode = new ContainerCode(CODE4);
		rContainer = new RefrigeratedContainer(rCode, GROSSWEIGHT_30, DEFAULT_TEMPERATURE);
		manifest.loadContainer(rContainer);
		
		ContainerCode gCode2 = new ContainerCode(CODE5);
		gContainer = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		
		final Integer EXPECTED_0 = 0;
		final Integer EXPECTED_1 = 1;
		
		assertEquals(EXPECTED_0, manifest.howHigh(gCode1));
		assertEquals(EXPECTED_0, manifest.howHigh(dCode));
		assertEquals(EXPECTED_0, manifest.howHigh(rCode));		
		assertEquals(EXPECTED_1, manifest.howHigh(gCode2));
	}

	
	/**
	 * Tests if howHigh detects the correct height of the loads in a 2 by 2
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_howHigh_sameHeightAndDiffrentStack() throws ManifestException, InvalidCodeException,
			InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_2, MAXHEIGHT_2, MAXWEIGHT);
		
		ContainerCode gCode1 = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		ContainerCode gCode2 = new ContainerCode(CODE2);
		gContainer = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		ContainerCode gCode3 = new ContainerCode(CODE4);
		gContainer = new GeneralGoodsContainer(gCode3, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		ContainerCode gCode4 = new ContainerCode(CODE5);
		gContainer = new GeneralGoodsContainer(gCode4, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		
		final Integer EXPECTED_0 = 0;
		final Integer EXPECTED_1 = 1;
		
		// stack index 0
		assertEquals(EXPECTED_0, manifest.howHigh(gCode1));
		assertEquals(EXPECTED_1, manifest.howHigh(gCode2));

		// stack index 1
		assertEquals(EXPECTED_0, manifest.howHigh(gCode3));
		assertEquals(EXPECTED_1, manifest.howHigh(gCode4));
	}
	
	/**
	 * Tests unified test if howHigh and whichStack are locating the containers correctly
	 * for all containers of the same type
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void checkStackAndHightForOneKind() throws ManifestException, InvalidCodeException,
			InvalidContainerException {
		manifest = new CargoManifest(NUMSTACKS_2, MAXHEIGHT_2, MAXWEIGHT);
		ContainerCode gCode1;

		gCode1 = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		
		ContainerCode gCode2;
		gCode2 = new ContainerCode(CODE2);
		gContainer = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		
		ContainerCode gCode3;
		gCode3 = new ContainerCode(CODE4);
		gContainer = new GeneralGoodsContainer(gCode3, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		
		ContainerCode gCode4;
		gCode4 = new ContainerCode(CODE5);
		gContainer = new GeneralGoodsContainer(gCode4, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		
		final Integer EXPECTED_0 = 0;
		final Integer EXPECTED_1 = 1;
		
		assertEquals(EXPECTED_0, manifest.whichStack(gCode1));
		assertEquals(EXPECTED_0, manifest.howHigh(gCode1));
		
		assertEquals(EXPECTED_0, manifest.whichStack(gCode2));
		assertEquals(EXPECTED_1, manifest.howHigh(gCode2));
		
		assertEquals(EXPECTED_1, manifest.whichStack(gCode3));
		assertEquals(EXPECTED_0, manifest.howHigh(gCode3));
		
		assertEquals(EXPECTED_1, manifest.whichStack(gCode4));
		assertEquals(EXPECTED_1, manifest.howHigh(gCode4));
	}
	
	/**
	 * Tests if howHigh and whichStack are locating the containers correctly for different
	 * type of containers
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void addOneEachAndCheckHeightAndStacks() throws ManifestException, InvalidCodeException,
			InvalidContainerException {
		ContainerCode gCode1 = new ContainerCode(CODE1);
		gContainer = new GeneralGoodsContainer(gCode1, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);

		ContainerCode dCode1 = new ContainerCode(CODE2);
		dContainer = new DangerousGoodsContainer(dCode1, GROSSWEIGHT_30, DEFAULT_CATEGORY);
		manifest.loadContainer(dContainer);

		ContainerCode rCode1 = new ContainerCode(CODE3);
		rContainer = new RefrigeratedContainer(rCode1, GROSSWEIGHT_30, DEFAULT_TEMPERATURE);
		manifest.loadContainer(rContainer);

		ContainerCode gCode2 = new ContainerCode(CODE4);
		gContainer = new GeneralGoodsContainer(gCode2, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		
		ContainerCode dCode2 = new ContainerCode(CODE5);
		dContainer = new DangerousGoodsContainer(dCode2, GROSSWEIGHT_30, DEFAULT_CATEGORY);
		manifest.loadContainer(dContainer);
		
		ContainerCode rCode2 = new ContainerCode(CODE6);
		rContainer = new RefrigeratedContainer(rCode2, GROSSWEIGHT_30, DEFAULT_TEMPERATURE);
		manifest.loadContainer(rContainer);
		
		final Integer EXPECTED_0 = 0;
		final Integer EXPECTED_1 = 1;
		final Integer EXPECTED_2 = 2;
		
		assertEquals(EXPECTED_0, manifest.whichStack(gCode1));
		assertEquals(EXPECTED_0, manifest.howHigh(gCode1));
		
		assertEquals(EXPECTED_1, manifest.whichStack(dCode1));
		assertEquals(EXPECTED_0, manifest.howHigh(dCode1));
		
		assertEquals(EXPECTED_2, manifest.whichStack(rCode1));
		assertEquals(EXPECTED_0, manifest.howHigh(rCode1));
		
		assertEquals(EXPECTED_0, manifest.whichStack(gCode2));
		assertEquals(EXPECTED_1, manifest.howHigh(gCode2));
		
		assertEquals(EXPECTED_1, manifest.whichStack(dCode2));
		assertEquals(EXPECTED_1, manifest.howHigh(dCode2));
		
		assertEquals(EXPECTED_2, manifest.whichStack(rCode2));
		assertEquals(EXPECTED_1, manifest.howHigh(rCode2));
	}
	
	/**
	 * Tests if an toArray returns null for an empty stack.
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_emptyStackToArray() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		assertTrue(manifest.toArray(0) == null);
		assertTrue(manifest.toArray(1) == null);
		assertTrue(manifest.toArray(2) == null);
	}
	
	/**
	 * Tests if toArray throws an exception when there is no stack in the index
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void test_stackNoIsGreaterThanSize() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		manifest.toArray(3);
	}
	
	/**
	 * Tests if a single item can be returned through toArray
	 * 
	 * @author Jieun Jang - N8828041
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_toArrayOneItem() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		code = new ContainerCode(CODE1);
		
		final int LENGTH = 1;
		gContainer = new GeneralGoodsContainer(code, GROSSWEIGHT_30);
		manifest.loadContainer(gContainer);
		assertEquals(LENGTH, manifest.toArray(0).length);
	}
	
	/**
	 * Basic testing for the toString and its overload
	 * 
	 * @author Thuan Nguyen - N7195508
	 * @throws InvalidContainerException
	 * @throws InvalidCodeException
	 * @throws ManifestException
	 */
	@Test
	public void test_toString() throws InvalidContainerException, InvalidCodeException, ManifestException {
		final Integer CONT_WEIGHT = 4;
		final Integer CONT_TEMP = 0;
		final Integer CONT_CAT = 3;
		
		FreightContainer dCont1 = new DangerousGoodsContainer(new ContainerCode(CODE1), CONT_WEIGHT, CONT_CAT);
		FreightContainer rCont1 = new RefrigeratedContainer(new ContainerCode(CODE2), CONT_WEIGHT, CONT_TEMP);
		
		FreightContainer gCont1 = new GeneralGoodsContainer(new ContainerCode(CODE3), CONT_WEIGHT);
		FreightContainer gCont2 = new GeneralGoodsContainer(new ContainerCode(CODE4), CONT_WEIGHT);
		
		manifest.loadContainer(dCont1);
		manifest.loadContainer(rCont1);
		manifest.loadContainer(gCont1);
		manifest.loadContainer(gCont2);
		
		String expected = START_BARS + CODE1 + END_BARS + NEW_LINE 
				+ START_BARS + CODE2 + END_BARS	+ NEW_LINE 
				+ START_BARS + CODE3 + MID_BARS + CODE4 + END_BARS + NEW_LINE;

		assertTrue(expected.equals(manifest.toString()));
		
		expected = START_BARS + CODE1 + END_BARS + NEW_LINE 
				+ START_BARS + CODE2 + END_BARS	+ NEW_LINE 
				+ START_FOUND_BARS + CODE3 + END_FOUND_BARS + SPACE 
				+ CODE4 + END_BARS + NEW_LINE;
		
		assertTrue(expected.equals(manifest.toString(new ContainerCode(CODE3))));
		
		expected = START_BARS + CODE1 + END_BARS + NEW_LINE 
				+ START_BARS + CODE2 + END_BARS	+ NEW_LINE 
				+ START_BARS + CODE3 + SPACE + START_FOUND_BARS + CODE4 + END_FOUND_BARS + NEW_LINE;
		
		assertTrue(expected.equals(manifest.toString(new ContainerCode(CODE4))));		
	}
	
	/**
	 * Tests if toArray returns the correct items and length of items in a random order.
	 * 
	 * @author Thuan Nguyen - N7195508
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void test_toArray() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		final Integer HEIGHT = 10;
		final Integer STACKS = 10;
		final Integer WEIGHT = 1000;
		final Integer CONT_WEIGHT = 4;
		final Integer CONT_TEMP = 0;
		final Integer CONT_CAT = 3;

		//setup random containers
		manifest = new CargoManifest(STACKS, HEIGHT, WEIGHT);
		
		ContainerCode gCode1 = new ContainerCode("AAAU0000000");
		ContainerCode gCode2 = new ContainerCode("AAAU0000011");
		ContainerCode gCode3 = new ContainerCode("AAAU0000022");
		ContainerCode gCode4 = new ContainerCode("AAAU0000033");
		ContainerCode gCode5 = new ContainerCode("AAAU0000044");
		
		ContainerCode dCode1 = new ContainerCode("AAAU0000066");
		ContainerCode dCode2 = new ContainerCode("AAAU0000077");
		ContainerCode dCode3 = new ContainerCode("AAAU0000088");
		
		ContainerCode rCode1 = new ContainerCode("AAAU0000099");
		ContainerCode rCode2 = new ContainerCode("AAAU0000101");
		ContainerCode rCode3 = new ContainerCode("AAAU0000202");
		ContainerCode rCode4 = new ContainerCode("AAAU0000303");
		
		FreightContainer gCont1 = new GeneralGoodsContainer(gCode1, CONT_WEIGHT);
		FreightContainer gCont2 = new GeneralGoodsContainer(gCode2, CONT_WEIGHT);
		FreightContainer gCont3 = new GeneralGoodsContainer(gCode3, CONT_WEIGHT);	
		FreightContainer gCont4 = new GeneralGoodsContainer(gCode4, CONT_WEIGHT);
		FreightContainer gCont5 = new GeneralGoodsContainer(gCode5, CONT_WEIGHT);

		FreightContainer dCont1 =  new DangerousGoodsContainer(dCode1, CONT_WEIGHT, CONT_CAT);
		FreightContainer dCont2 =  new DangerousGoodsContainer(dCode2, CONT_WEIGHT, CONT_CAT);
		FreightContainer dCont3 = new DangerousGoodsContainer(dCode3, CONT_WEIGHT, CONT_CAT);
		
		FreightContainer rCont1 =  new RefrigeratedContainer(rCode1, CONT_WEIGHT, CONT_TEMP);
		FreightContainer rCont2 =  new RefrigeratedContainer(rCode2, CONT_WEIGHT, CONT_TEMP);
		FreightContainer rCont3 = new RefrigeratedContainer(rCode3, CONT_WEIGHT, CONT_TEMP);
		FreightContainer rCont4 = new RefrigeratedContainer(rCode4, CONT_WEIGHT, CONT_TEMP);
		
		//irregular loads
		manifest.loadContainer(gCont1);
		manifest.loadContainer(gCont2);
		manifest.loadContainer(gCont3);
		
		manifest.loadContainer(dCont1);
		manifest.loadContainer(dCont2);
		manifest.loadContainer(dCont3);
		
		manifest.loadContainer(gCont4);
		
		manifest.loadContainer(rCont1);
		manifest.loadContainer(rCont2);
		
		manifest.loadContainer(gCont5);
		
		manifest.loadContainer(rCont3);
		manifest.loadContainer(rCont4);
		
		//extracting array
		FreightContainer[] gArray = manifest.toArray(0);
		FreightContainer[] dArray = manifest.toArray(1);
		FreightContainer[] rArray = manifest.toArray(2);
		
		final int GLENGTH = 5;
		final int DLENGTH = 3;
		final int RLENGTH = 4;
				
		assertEquals(GLENGTH, gArray.length);
		assertEquals(DLENGTH, dArray.length);
		assertEquals(RLENGTH, rArray.length);
		
		//comparing if containers are in the correct order
		assertTrue(gCode1.equals(gArray[0].getCode()));
		assertTrue(gCode2.equals(gArray[1].getCode()));
		assertTrue(gCode3.equals(gArray[2].getCode()));
		assertTrue(gCode4.equals(gArray[3].getCode()));
		assertTrue(gCode5.equals(gArray[4].getCode()));
		
		assertTrue(dCode1.equals(dArray[0].getCode()));
		assertTrue(dCode2.equals(dArray[1].getCode()));
		assertTrue(dCode3.equals(dArray[2].getCode()));
		
		assertTrue(rCode1.equals(rArray[0].getCode()));
		assertTrue(rCode2.equals(rArray[1].getCode()));
		assertTrue(rCode3.equals(rArray[2].getCode()));
		assertTrue(rCode4.equals(rArray[3].getCode()));
	}
}