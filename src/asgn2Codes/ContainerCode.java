package asgn2Codes;

import java.util.regex.Pattern;

import asgn2Exceptions.InvalidCodeException;

/* Note to self:
 * Use option "-noqualifier java.lang" when generating Javadoc from this
 * file so that, for instance, we get "String" instead of
 * "java.lang.String".
 */

/**
 * This class provides cargo container codes in a format similar to that
 * prescribed by international standard ISO 6346.  (The only difference
 * is that we use a simpler algorithm for calculating the check digit.)
 * <p>
 * A container code is an 11-character string consisting of the following
 * fields:
 * <ul>
 * <li>
 * An <em>Owner Code</em> which uniquely identifies the company that owns
 * the container.  The owner code consists of three upper-case letters.
 * (To ensure uniqueness in practice, owner codes must be registered at
 * the <em>Bureau International des Containers</em> in Paris.)
 * </li>
 * <li>
 * A <em>Category Identifier</em> which identifies the type of container.
 * The identifier is one of three letters, 'U', 'J' or 'Z'.  For our
 * purposes the category identifier is <em>always</em> 'U', which denotes a
 * freight container.
 * </li>
 * <li>
 * A <em>Serial Number</em> used by the owner to uniquely identify the
 * container.  The number consists of six digits.
 * </li>
 * <li>
 * A <em>Check Digit</em> used to ensure that the number has not been
 * transcribed incorrectly.  It consists of a single digit derived from the
 * other 10 characters according to a specific algorithm, described below.
 * </li>
 * </ul>
 * <p>
 * <strong>Calculating the check digit:</strong> ISO 6346 specifies a
 * particular algorithm for deriving the check digit from the
 * other 10 characters in the code.  However, because this algorithm is
 * rather complicated, we use a simpler version for this assignment.
 * Our algorithm works as follows:
 * <ol>
 * <li>
 * Each of the 10 characters in the container code (excluding the check
 * digit) is treated as a number.  The digits '0' to '9' each have
 * their numerical value.  The upper case alphabetic letters 'A' to
 * 'Z' are treated as the numbers '0' to '25', respectively.
 * </li>
 * <li>
 * These 10 numbers are added up.
 * </li>
 * <li>
 * The check digit is the least-significant digit in the sum produced
 * by the previous step.
 * </li>
 * </ol>
 * For example, consider container code <code>MSCU6639871</code> which
 * has owner code <code>MSC</code>, category identifier <code>U</code>,
 * serial number <code>663987</code> and check digit <code>1</code>.  We can 
 * confirm that this code is valid by treating the letters as numbers (e.g.,
 * '<code>M</code>' is 12, '<code>S</code>' is 18, etc) and calculating the
 * following sum.
 * <p>
 * <center>
 * 12 + 18 + 2 + 20 + 6 + 6 + 3 + 9 + 8 + 7 = 91
 * </center>
 * <p>
 * The check digit is then the least-sigificant digit in the number 91,
 * i.e., '<code>1</code>', thus confirming that container code 
 * <code>MSCU6639871</code> is valid.
 * 
 * @author Thuan Nguyen - N7195508 
 * @version 1.0
 */ 
public class ContainerCode {
	private String containerCode;
	
	/**
	 * Constructs a new container code.
	 * 
	 * @author Thuan Nguyen - N7195508
	 * @param code the container code as a string
	 * @throws InvalidCodeException if the container code is not eleven characters long;
	 * if the Owner Code does not consist of three upper-case letters; if the
	 * Category Identifier is not 'U'; if the Serial Number does not consist
	 * of six digits; or if the Check Digit is incorrect.
	 */
	public ContainerCode(String code) throws InvalidCodeException {
		verifyFormat(code);
		this.containerCode = code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.containerCode;
	}

	/**
	 * Returns true iff the given object is a container code and has an identical value to
	 * this code.
	 * 
	 * @author Thuan Nguyen - N7195508
	 * @param obj
	 * an object
	 * @return true if the given object is a container code with the same string value as
	 * this code, false otherwise
	 */
	@Override
	public boolean equals(Object obj) {
		return this.toString().equals(obj.toString());
	}

	/**
	 * Verify if the code complies to the formatting of the ISO 6346
	 * 
	 * @author Thuan Nguyen - N7195508
	 * @param code
	 * @return <code>true</code> if first 3 characters are capital letters, container
	 * identifier is a 'U', and the remaining 7 characters are numbers. Otherwise,
	 * false.
	 * @throws InvalidCodeException
	 */
	private void verifyFormat(String code) throws InvalidCodeException {
		final int CODE_LENGTH = 11;
		final String ownerCodeFormat = "[A-Z]{3}.*";
		final String categoryFormat = ".{3}U.*";
		final String serialFormat = ".{4}[0-9]{6}.*";
		final String checkDigitFormat = ".{10}[0-9]";
		
		if (code.length() != CODE_LENGTH) {
			throw new InvalidCodeException("Invalid Code length");
		} if (!Pattern.matches(ownerCodeFormat, code)) {
			throw new InvalidCodeException("Invalid Owner format");
		} if (!Pattern.matches(categoryFormat, code)) {
			throw new InvalidCodeException("Invalid Category format");
		} if (!Pattern.matches(serialFormat, code)) {
			throw new InvalidCodeException("Invalid Serial format");
		} if (!Pattern.matches(checkDigitFormat, code)) {
			throw new InvalidCodeException("Invalid Check Digit format");
		} if (!verifyCheckDigit(code)) {
			throw new InvalidCodeException("Invalid Container Code");
		}
		
		//I'd use the code below if the Assignment wasn't nagging for so many
		//exceptions to be thrown. 
		
//		final String containerFormat = "[A-Z]{3}U[0-9]{7}";
//		if(!Pattern.matches(containerFormat, code)){
//			throw new InvalidCodeException("Invalid Container Code");
//		}
	}
	
	/**
	 * Verify if the checkbit complies to ISO 6346
	 * 
	 * @author Thuan Nguyen - N7195508
	 * @param code
	 * @return <code>true</code> if the check bit is the correct value
	 */
	private boolean verifyCheckDigit(String code) {
		final int SERIALINDEX = 4;

		int sum = 0;
		for (int i = 0; i < SERIALINDEX; i++) {
			sum += code.charAt(i) - 'A';
		}
		for (int i = SERIALINDEX; i < code.length() - 1; i++) {
			sum += code.charAt(i) - '0';
		}

		return sum % 10 == Integer.parseInt(code.substring(code.length() - 1));
	}
}

