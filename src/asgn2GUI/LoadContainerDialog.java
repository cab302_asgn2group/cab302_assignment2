package asgn2GUI;

import java.awt.CardLayout;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.FreightContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.CargoException;

/**
 * Creates a dialog box allowing the user to enter information required for loading a container.
 *
 * @author Thuan Nguyen - N7195508
 */
@SuppressWarnings("serial")
public class LoadContainerDialog extends AbstractDialog implements ActionListener, ItemListener {
    //Dialog Dimensions
	private static final int HEIGHT = 200;
    private static final int WIDTH = 350;

    private JPanel pnlCards;
    
    //Text field for entries
    private JTextField txtDangerousGoodsType;
    private JTextField txtTemperature;
    private JTextField txtWeight;
    private JTextField txtCode;

    private JComboBox<String> cbType;
    private static String comboBoxItems[] = new String[] { "Dangerous Goods", "General Goods", "Refrigerated Goods" };
    //private String cbEvent;
    private FreightContainer container;

    /**
     * Constructs a modal dialog box that gathers information required for loading a container.
     *
     * @param parent the frame which created this dialog box.
     */
    private LoadContainerDialog(JFrame parent) {
        super(parent, "Container Information", WIDTH, HEIGHT);
        setResizable(false);
        setName("Container Information");
    }

    /**
     * @see AbstractDialog.createContentPanel()
     */
    @Override
    protected JPanel createContentPanel() {
        createCards();
        GridBagConstraints constraints = new GridBagConstraints();

        // Defaults
        constraints.fill = GridBagConstraints.BOTH;
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.weightx = 100;
        constraints.weighty = 100;

        JPanel pnlContent = new JPanel();
        pnlContent.setLayout(new GridBagLayout());
        addToPanel(pnlContent, createCommonControls(), constraints, 0, 0, 2, 1);
        addToPanel(pnlContent, pnlCards, constraints, 0, 2, 2, 1);

        return pnlContent;
    }

    /**
     * Creates and layout the assets for load container dialog
     * 
     * @return The panel for the controls on load container dialog
     */
    private JPanel createCommonControls() {
        JPanel pnlCommonControls = new JPanel();
        pnlCommonControls.setLayout(new GridBagLayout());

        // add comopents to grid
        GridBagConstraints constraints = new GridBagConstraints();

        // Defaults
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.EAST;
        constraints.weightx = 100;
        constraints.weighty = 100;
        constraints.insets = new Insets(3, 3, 3, 3);

        //Don't modify - START 
        cbType = new JComboBox<String>(comboBoxItems);
        cbType.setEditable(false);
        cbType.addItemListener(this);
        cbType.setName("Container Type");
        //Don't modify - END 

        txtWeight = createTextField(5, "Container Weight");
        txtCode = createTextField(11, "Container Code");

        addToPanel(pnlCommonControls, new JLabel("Container Type: "), constraints, 0, 0, 2, 1);
        addToPanel(pnlCommonControls, new JLabel("Container Code: "), constraints, 0, 2, 2, 1);
        addToPanel(pnlCommonControls, new JLabel("Container Weight: "), constraints, 0, 4, 2, 1);
        constraints.anchor = GridBagConstraints.WEST;
        addToPanel(pnlCommonControls, cbType, constraints, 3, 0, 2, 1);
        addToPanel(pnlCommonControls, txtCode, constraints, 3, 2, 2, 1);
        addToPanel(pnlCommonControls, txtWeight, constraints, 3, 4, 2, 1);

        return pnlCommonControls;
    }

    /*
     * Factory method to create a named JTextField
     */
    private JTextField createTextField(int numColumns, String name) {
        JTextField text = new JTextField();
        text.setColumns(numColumns);
        text.setName(name);
        return text;
    }

    /**
     * Create the cards for combobox items
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void createCards() {
        GridBagConstraints constraints = new GridBagConstraints();

        // Defaults
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.EAST;
        constraints.weightx = 100;
        constraints.weighty = 100;
        constraints.insets = new Insets(3, 3, 3, 3);
        
        //create cards
        JPanel cardGeneralGoods = new JPanel();
        cardGeneralGoods.setLayout(new GridBagLayout());
        
        JPanel cardDangerousGoods = new JPanel();
        cardDangerousGoods.setLayout(new GridBagLayout());
        txtDangerousGoodsType = createTextField(5, "Goods Category");
        
        JPanel cardRefrigeratedGoods = new JPanel();
        cardRefrigeratedGoods.setLayout(new GridBagLayout());        
        txtTemperature = createTextField(5, "Temperature");
        
        //add cards
        addToPanel(cardDangerousGoods,
                new JLabel(txtDangerousGoodsType.getName() + ": "), 
                constraints, 0, 0, 2, 1);
        addToPanel(cardRefrigeratedGoods,
                new JLabel(txtTemperature.getName() + ": "), 
                constraints, 0, 0, 2, 1);
        
        constraints.anchor = GridBagConstraints.WEST;
        addToPanel(cardDangerousGoods, txtDangerousGoodsType, constraints, 3, 0, 2, 1);
        addToPanel(cardRefrigeratedGoods, txtTemperature, constraints, 3, 0, 2, 1);        
        
        pnlCards = new JPanel(new CardLayout());
        pnlCards.add(cardDangerousGoods, comboBoxItems[0]);
        pnlCards.add(cardGeneralGoods, comboBoxItems[1]);
        pnlCards.add(cardRefrigeratedGoods, comboBoxItems[2]);
    }

    /**
     * @see java.awt.ItemListener.itemStateChanged(ItemEvent e)
     */
    @Override
    public void itemStateChanged(ItemEvent event) {
        CardLayout cl = (CardLayout) pnlCards.getLayout();
        cl.show(pnlCards, (String) event.getItem());
    }

    /**
     * @see AbstractDialog.dialogDone()
     */
    @Override
    protected boolean dialogDone() {
    	try{
    	    if(txtCode.getText().isEmpty()) {
    	        throw new NumberFormatException("No value for Container Code.");
    	    }
    	    if(txtWeight.getText().isEmpty()) {
                throw new NumberFormatException("No value for Weight.");
            }
    		if(cbType.getSelectedItem() == null || cbType.getSelectedItem() == comboBoxItems[0]){
    		    if(txtDangerousGoodsType.getText().isEmpty()) {
    		        throw new NumberFormatException("No value for Goods Category.");
    		    }
	    		container = new DangerousGoodsContainer(new ContainerCode(txtCode.getText()),
	    				Integer.parseInt(txtWeight.getText()),
	    				Integer.parseInt(txtDangerousGoodsType.getText()));
	    		return true;
	    	}
	    	if(cbType.getSelectedItem() == comboBoxItems[1]){
	    		container = new GeneralGoodsContainer(new ContainerCode(txtCode.getText()),
	    				Integer.parseInt(txtWeight.getText()));
	    		return true;
	    	}	    	
	    	if(cbType.getSelectedItem() == comboBoxItems[2]){
	    	    if(txtTemperature.getText().isEmpty()) {
                    throw new NumberFormatException("No value for Temperature.");
                }
	    		container = new RefrigeratedContainer(new ContainerCode(txtCode.getText()), 
	    				Integer.parseInt(txtWeight.getText()),
	    				Integer.parseInt(txtTemperature.getText()));
	    		return true;
	    	}   	
    	} catch(CargoException e){
    		JOptionPane.showMessageDialog(this, e.getMessage(), 
    				"Cargo Error", JOptionPane.ERROR_MESSAGE);
    	} catch(NumberFormatException e){
    		JOptionPane.showMessageDialog(this, 
    				"Invalid Input: " + e.getMessage() + "\nInput must be an Integer.", 
    				"Input Error", JOptionPane.ERROR_MESSAGE);
    	} 
		return false;
    }

    /**
     * Shows a <code>LoadContainerDialog</code> for user interaction.
     *
     * @param parent - The parent <code>JFrame</code> which created this dialog box.
     * @return a <code>FreightContainer</code> instance with valid values.
     */
    public static FreightContainer showDialog(JFrame parent) {
    	LoadContainerDialog lcd = new LoadContainerDialog(parent);
    	lcd.setVisible(true);
    	return lcd.container;
    }
}
