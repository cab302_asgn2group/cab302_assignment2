package asgn2GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import asgn2Codes.ContainerCode;
import asgn2Containers.FreightContainer;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * The main window for the Cargo Manifest graphics application.
 *
 * @author Thuan Nguyen - N7195508
 */
@SuppressWarnings("serial")
public class CargoFrame extends JFrame {
    //Window Dimensions
	private static final int WIDTH = 600;
    private static final int HEIGHT = 400;

    //Buttons for operation on cargo
    private JButton btnLoad;
    private JButton btnUnload;
    private JButton btnFind;
    private JButton btnNewManifest;

    private CargoCanvas canvas;

    //Window subsegments.
    private JPanel pnlControls;
    private JPanel pnlDisplay;
    private JScrollPane scrollPane;
    private CargoManifest cargo;

    /**
     * Constructs the GUI.
     * 
     * @author Thuan Nguyen - N7195508
     * @param title The frame title to use.
     * @throws HeadlessException from JFrame.
     */
    public CargoFrame(String title) throws HeadlessException {
        super(title);        
        constructorHelper();
        disableButtons();
        redraw();
        setVisible(true);
    }

    /**
     * Initialises the container display area.
     *
     * @author Thuan Nguyen - N7195508
     * @param cargo The <code>CargoManifest</code> instance containing necessary state for display.
     */
    private void setCanvas(CargoManifest cargo) {
    	if (canvas != null) {
            pnlDisplay.remove(canvas);
        }
        if (cargo == null) {
            disableButtons();
        } else {            
        	canvas = new CargoCanvas(cargo);
            pnlDisplay.add(canvas, BorderLayout.CENTER);
            enableButtons();
        }
    }

    /**
     * Enables buttons for user interaction.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void enableButtons() {
    	btnLoad.setEnabled(true);
    	btnUnload.setEnabled(true);
    	btnFind.setEnabled(true);
    	btnNewManifest.setEnabled(true);     
    }

    /**
     * Disables buttons from user interaction.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void disableButtons() {
    	btnLoad.setEnabled(false);
    	btnUnload.setEnabled(false);
    	btnFind.setEnabled(false);   
    }
    
    /**
     * Initialises and lays out GUI components.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void constructorHelper() {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
                //assist with updating the scroll bar when cargo
                //cannot fit within current space.
                canvas.updateUI();
            }

            @Override
            public void componentHidden(ComponentEvent e) {
                //no need to override this method
            }

            @Override
            public void componentMoved(ComponentEvent e) {
                //no need to override this method
            }

            @Override
            public void componentShown(ComponentEvent e) {
                //no need to override this method
            }
        });
        
        //Create buttons and set interactive actions.
        btnLoad = createButton("Load", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                        CargoFrame.this.doLoad();
                    }
                };
                SwingUtilities.invokeLater(doRun);
            }
        });
        btnUnload = createButton("Unload", new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                    	CargoFrame.this.doUnload();
                    }
                };
                SwingUtilities.invokeLater(doRun);
			}
        });
        btnFind = createButton("Find", new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                    	CargoFrame.this.doFind();
                    }
                };
                SwingUtilities.invokeLater(doRun);
			}
        });
        btnNewManifest = createButton("New Manifest", new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                    	CargoFrame.this.setNewManifest();
                    }
                };
                SwingUtilities.invokeLater(doRun);
			}
        });
        
        //Set panel controls and display area
        pnlControls = createControlPanel();         
        pnlDisplay = new JPanel();   
        canvas = new CargoCanvas(cargo);
        pnlDisplay.setLayout(new BorderLayout());
        pnlDisplay.add(canvas, BorderLayout.CENTER);
        pnlDisplay.setBackground(Color.WHITE);
        
        scrollPane = new JScrollPane(pnlDisplay);
        scrollPane.setBackground(Color.WHITE);
        this.getContentPane().add(scrollPane, BorderLayout.CENTER);        
        this.getContentPane().add(pnlControls, BorderLayout.SOUTH);
        setCanvas(cargo);
        repaint();
    }

    /**
     * Creates a JPanel containing user controls (buttons).
     * 
     * @author Thuan Nguyen - N7195508
     * @return User control panel.
     */
    private JPanel createControlPanel() {
    	JPanel pnlControls = new JPanel();    
    	pnlControls.setLayout(new GridBagLayout());
    	    
	    GridBagConstraints constraints = new GridBagConstraints(); 
	    
	    //Defaults
	    constraints.insets = new Insets(3, 3, 3, 3);
	    pnlControls.add(btnLoad, constraints);
	    pnlControls.add(btnUnload, constraints);
	    pnlControls.add(btnFind, constraints);
	    pnlControls.add(btnNewManifest, constraints);
		return pnlControls;   
    }

    /**
     * Factory method to create a JButton and add its ActionListener.
     * 
     * @author Thuan Nguyen - N7195508
     * @param name The text to display and use as the component's name.
     * @param btnListener The ActionListener to add.
     * @return A named JButton with ActionListener added.
     */
    private JButton createButton(String name, ActionListener btnListener) {
        JButton btn = new JButton(name);
        btn.setName(name);
        btn.addActionListener(btnListener);
        return btn;
    }

    /**
     * Initiate the New Manifest dialog which sets the instance of CargoManifest to work with.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void setNewManifest() {
    	CargoManifest cargo = ManifestDialog.showDialog(this);
    	if(cargo != null){
            this.resetCanvas();
    		this.cargo = cargo;
        	this.setCanvas(this.cargo);
    	}
        this.redraw();
    }

    /**
     * Turns off container highlighting when an action other than Find is initiated.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void resetCanvas() {
	    canvas.disableHighlight();
    }

    /**
     * Initiates the Load Container dialog and attempt to load container.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void doLoad() {
    	FreightContainer fc = LoadContainerDialog.showDialog(this);
    	if(fc != null){
    		try{
                this.resetCanvas();
    			cargo.loadContainer(fc);
    		} catch(ManifestException e){
    			JOptionPane.showMessageDialog(this, e.getMessage(), 
        				"Cargo Error", JOptionPane.ERROR_MESSAGE);
    		} 
    	}	
        this.redraw();
    }

    /**
     * Initiates the Container Code dialog and attempt to unload.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void doUnload() {
    	ContainerCode cc = canvas.getHighlightedContainer();
    	if(cc != null){
    	    //Auto-fill ContainerCode Dialog with the code of the highlighted container
    		cc = ContainerCodeDialog.showDialog(this, cc);
    	} else {
    	    //Normal ContainerCode Dialog when no contaner highlighted
    		cc = ContainerCodeDialog.showDialog(this);    		
		}
    	
    	//If user entered in a valid code, try to unloadContainer
    	if(cc != null){
    	    try{
                cargo.unloadContainer(cc);
                this.resetCanvas();
            } catch(ManifestException e){
                JOptionPane.showMessageDialog(this, e.getMessage(),
                        "Cargo Error", JOptionPane.ERROR_MESSAGE);
            }
    	}
    	this.redraw();
    }

    /**
     * Initiates Container Code dialog and set highlight.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void doFind() {   
    	ContainerCode toFind = ContainerCodeDialog.showDialog(this);
    	if(toFind != null){
    		if(cargo.whichStack(toFind) != null){
    	        this.resetCanvas();
	    		canvas.setToFind(toFind);
    		} else {
    			JOptionPane.showMessageDialog(this, "Container does not exist",
    					"Cargo Error", JOptionPane.ERROR_MESSAGE);
    		}
    	}
    	this.redraw();
    }
    
    /**
     * Updates the frame and canvas UI.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void redraw() {
        invalidate();
        validate();
        repaint();
        canvas.updateUI();
    }
}

/* COMMENTED CODE BELOW WAS FOR MANUAL TESTING */
/* NOT NEEDED FOR PROGRAM FUNCTIONALITY        */

//@SuppressWarnings("unused")
//private CargoManifest fillTest(){
//  CargoManifest c = null;
//  try {
//      int row = 20;
//      int col = 20;
//      c = new CargoManifest(row, col, row * col * 4);
//      for(int i = 0; i < row * col; i++){
//          String initCode = "AAAU";
//          String tempCode = initCode + String.format("%06d", i);
//          tempCode = tempCode + makeCheckBit(tempCode).toString();
//          FreightContainer fc = new GeneralGoodsContainer(new ContainerCode(tempCode), 4);
//          c.loadContainer(fc);
//      }
//      
//      System.out.println(c.toString());
//  } catch (ManifestException e) {
//      // TODO Auto-generated catch block
//      e.printStackTrace();
//  } catch (InvalidContainerException e) {
//      // TODO Auto-generated catch block
//      e.printStackTrace();
//  } catch (InvalidCodeException e) {
//      // TODO Auto-generated catch block
//      e.printStackTrace();
//  }  
//  return c;
//}
//
//private Integer makeCheckBit(String currentCode){
//    if(currentCode.length() > 10){
//        System.out.println("Code is too long");
//    }
//    int num = 0;
//    for(int i = 0; i < 4; i++){
//        num += currentCode.charAt(i) - 'A';
//    }
//    for(int i = 4; i < 10; i++){
//        num += currentCode.charAt(i) - '0';
//    }
//    return num%10;
//}