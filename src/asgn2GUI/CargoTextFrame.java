package asgn2GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import asgn2Codes.ContainerCode;
import asgn2Containers.FreightContainer;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * The main window for the Cargo Manifest Text application.
 *
 * @author Thuan Nguyen - N7195508
 */
@SuppressWarnings("serial")
public class CargoTextFrame extends JFrame {
    //Window Dimensions
	private static final int WIDTH = 600;
    private static final int HEIGHT = 400;

    //Buttons for operation on cargo
    private JButton btnLoad;
    private JButton btnUnload;
    private JButton btnFind;
    private JButton btnNewManifest;

    private CargoTextArea canvas;

    //Window subsegments
    private JPanel pnlControls;
    private JPanel pnlDisplay;

    private CargoManifest cargo;

    /**
     * Constructs the GUI.
     *
     * @author Thuan Nguyen - N7195508
     * @param title The frame title to use.
     * @throws HeadlessException from JFrame.
     */
    public CargoTextFrame(String frameTitle) throws HeadlessException {
        super(frameTitle);
        constructorHelper();
        disableButtons();
        setVisible(true);
    }

    /**
     * Initialises the container display area.
     * 
     * @author Thuan Nguyen - N7195508
     * @param cargo The <code>CargoManifest</code> instance containing necessary state for display.
     */
    private void setCanvas(CargoManifest cargo) {
        if (canvas != null) {
            pnlDisplay.remove(canvas);
        }
        if (cargo == null) {
            disableButtons();
        } else {            
        	canvas = new CargoTextArea(cargo);
            pnlDisplay.add(canvas, BorderLayout.CENTER);
            pnlDisplay.setBackground(Color.WHITE);
            enableButtons();
        }
    }

    /**
     * Enables buttons for user interaction.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void enableButtons() {
    	btnLoad.setEnabled(true);
    	btnUnload.setEnabled(true);
    	btnFind.setEnabled(true);
    	btnNewManifest.setEnabled(true); 
	}

    /**
     * Disables buttons from user interaction.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void disableButtons() {
    	btnLoad.setEnabled(false);
    	btnUnload.setEnabled(false);
    	btnFind.setEnabled(false);
    }

    /**
     * Initialises and lays out GUI components.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void constructorHelper() {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Create buttons and set interactive actions.
        btnLoad = createButton("Load", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                        CargoTextFrame.this.resetCanvas();
                        CargoTextFrame.this.doLoad();
                    }
                };
                SwingUtilities.invokeLater(doRun);
            }
        });
        btnUnload = createButton("Unload", new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                        CargoTextFrame.this.resetCanvas();
                        CargoTextFrame.this.doUnload();
                    }
                };
                SwingUtilities.invokeLater(doRun);
			}
        });
        btnFind = createButton("Find", new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                        CargoTextFrame.this.doFind();
                    }
                };
                SwingUtilities.invokeLater(doRun);
			}
        });
        btnNewManifest = createButton("New Manifest", new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                        CargoTextFrame.this.resetCanvas();
                        CargoTextFrame.this.setNewManifest();
                    }
                };
                SwingUtilities.invokeLater(doRun);
			}
        });

        //Set panel controls and display area;
        pnlControls = createControlPanel();   
        pnlDisplay = new JPanel();   
        canvas = new CargoTextArea(cargo);
        pnlDisplay.setLayout(new BorderLayout());
        pnlDisplay.add(canvas, BorderLayout.CENTER);
        pnlDisplay.setBackground(Color.WHITE);
        
        JScrollPane scrollPane = new JScrollPane(pnlDisplay);
        this.getContentPane().add(scrollPane, BorderLayout.CENTER);        
        this.getContentPane().add(pnlControls, BorderLayout.SOUTH);
        setCanvas(cargo);
        repaint();
    }

    /**
     * Creates a JPanel containing user controls (buttons).
     *
     * @author Thuan Nguyen - N7195508
     * @return User control panel.
     */
    private JPanel createControlPanel() {    	
    	JPanel pnlControls = new JPanel();    
    	pnlControls.setLayout(new GridBagLayout());
    	    
	    GridBagConstraints constraints = new GridBagConstraints(); 
	    
	    //Defaults
	    constraints.insets = new Insets(3, 3, 3, 3);
	    
	    pnlControls.add(btnLoad, constraints);
	    pnlControls.add(btnUnload, constraints);
	    pnlControls.add(btnFind, constraints);
	    pnlControls.add(btnNewManifest, constraints);
		return pnlControls;
    }

    /**
     * Factory method to create a JButton and add its ActionListener.
     *
     * @author Thuan Nguyen - N7195508
     * @param name The text to display and use as the component's name.
     * @param btnListener The ActionListener to add.
     * @return A named JButton with ActionListener added.
     */
    private JButton createButton(String name, ActionListener btnListener) {
        JButton btn = new JButton(name);
        btn.setName(name);
        btn.addActionListener(btnListener);
        return btn;
    }

    /**
     * Initiate the New Manifest dialog which sets the instance of CargoManifest to work with.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void setNewManifest() {
    	CargoManifest cargo = ManifestDialog.showDialog(this);
    	if(cargo != null){
    		this.cargo = cargo;
        	this.setCanvas(this.cargo);
        	this.redraw();
    	}
    	repaint();
    }

    /**
     * Turns off container highlighting when an action other than Find is initiated.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void resetCanvas() {
    	if(cargo != null){
    		this.canvas.updateDisplay();
    	}
    }

    /**
     * Initiates the Load Container dialog and load container when successful.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void doLoad() {
    	FreightContainer freightContainer = LoadContainerDialog.showDialog(this);
    	if(freightContainer != null){
    		try{
    			cargo.loadContainer(freightContainer);
    		} catch(ManifestException e){
    			JOptionPane.showMessageDialog(this, e.getMessage(), 
        				"Error", JOptionPane.ERROR_MESSAGE);
    		} 
    	}
    	this.redraw();
    }

    /**
     * Initiates the Unload Container dialog and unload when successful.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void doUnload() {
    	ContainerCode cc = ContainerCodeDialog.showDialog(this);
    	if(cc != null){
    		try{
    			cargo.unloadContainer(cc);
    		} catch(ManifestException e){
    			JOptionPane.showMessageDialog(this, e.getMessage(),
    					"Error", JOptionPane.ERROR_MESSAGE);
    		}
    	}
    	this.redraw();
    }

    /**
     * Initiates the Find Container dialog.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void doFind() {
    	ContainerCode toFind = ContainerCodeDialog.showDialog(this);
    	if(toFind != null){
    		if(cargo.whichStack(toFind) != null){
	    		canvas.setToFind(toFind);
    		} else {
    			JOptionPane.showMessageDialog(this, "Container does not exist",
    					"Cargo Error", JOptionPane.ERROR_MESSAGE);
    		}
    	}  	
    	this.repaint();
    }

    /**
     * Updates the display area.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void redraw() {
    	this.canvas.updateDisplay();
    	this.repaint();
    }
    
}
