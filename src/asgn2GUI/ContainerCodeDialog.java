package asgn2GUI;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import asgn2Codes.ContainerCode;
import asgn2Exceptions.InvalidCodeException;

/**
 * Creates a dialog box allowing the user to enter a ContainerCode.
 *
 * @author Thuan Nguyen - N7195508
 */
@SuppressWarnings("serial")
public class ContainerCodeDialog extends AbstractDialog {
    //Dialog Dimensions
	private final static int WIDTH = 270;
    private final static int HEIGHT = 150;

    private JTextField txtCode;
    
////  HIGHLIGHTED lblErrorInfo because it's useless
    private JLabel lblErrorInfo;
    private ContainerCode code;

    /**
     * Constructs a modal dialog box that requests a container code.
     *
     * @author Thuan Nguyen - N7195508
     * @param parent the frame which created this dialog box.
     */
    private ContainerCodeDialog(JFrame parent) {
        super(parent, "Container Code", WIDTH, HEIGHT);
        setName("Container Dialog");
        setResizable(false);
    }
    
    /**
     * @see AbstractDialog.createContentPanel()
     */
    @Override
    protected JPanel createContentPanel() {
        JPanel toReturn = new JPanel();
        toReturn.setLayout(new GridBagLayout());

        // add components to grid
        GridBagConstraints constraints = new GridBagConstraints();
        
        // Defaults
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.CENTER;
        lblErrorInfo = new JLabel();        
        lblErrorInfo.setPreferredSize(new Dimension(230, 60));
        lblErrorInfo.setHorizontalAlignment(SwingConstants.CENTER);
        txtCode = new JTextField();
        txtCode.setColumns(11);
        txtCode.setName("Container Code");
        txtCode.getDocument().addDocumentListener(new DocumentListener() {
            //check for changes in txtCode
            @Override
            public void removeUpdate(DocumentEvent e) {
                validate();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                validate();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                validate();
            }
            
            //Attempts to validate the ContainerCode entered in the Container Code text field.
            private void validate() {
                //Disables submit when code verification throws an exception
            	try{
            		lblErrorInfo.setText("<HTML><FONT COLOR='green'><CENTER>Good to go!<CENTER></FONT></HTML>");
            		new ContainerCode(txtCode.getText());
            		enableSubmit();
            	} catch(InvalidCodeException e){
            		lblErrorInfo.setText("<HTML><FONT COLOR='red'> " + e.getMessage() + "</FONT></HTML>");
            		lblErrorInfo.repaint();
            		disableSubmit();
            	}
            }
        });
        //Layout components for messages and code entry        
        JLabel lblContCode = new JLabel(txtCode.getName() + ":  "); 
        
        JPanel codeEntry = new JPanel(new GridLayout(1, 2));
        
        lblContCode.setHorizontalAlignment(SwingConstants.RIGHT);
                
        codeEntry.add(lblContCode);
        codeEntry.add(txtCode);
        
        addToPanel(toReturn, lblErrorInfo, constraints, 0, 0, 2, 1);
        addToPanel(toReturn, codeEntry, constraints, 0, 2, 2, 1);
        
        disableSubmit();
        return toReturn;
    }

    @Override
    protected boolean dialogDone() {
        //returns true when entry is a valid container code
    	try{
    		code = new ContainerCode(txtCode.getText());
    	} catch(InvalidCodeException e){
    		JOptionPane.showMessageDialog(this, e.getMessage(), 
    				"Cargo Error", JOptionPane.ERROR_MESSAGE);
    		return false;
    	}
		return true;
    }

    /**
     * Shows the <code>ManifestDialog</code> for user interaction.
     *
     * @param parent - The parent <code>JFrame</code> which created this dialog box.
     * @return a <code>ContainerCode</code> instance with valid values.
     */
    public static ContainerCode showDialog(JFrame parent) {
    	ContainerCodeDialog ccd = new ContainerCodeDialog(parent);
    	ccd.setVisible(true);
    	return ccd.code;
    }
    
    /**
     * Shows the <code>ManifestDialog</code> for user interaction.
     *
     * @param parent - The parent <code>JFrame</code> which created this dialog box.
     * @return a <code>ContainerCode</code> instance with valid values.
     */
    public static ContainerCode showDialog(JFrame parent, ContainerCode highlightedContainer) {
    	ContainerCodeDialog ccd = new ContainerCodeDialog(parent);
    	ccd.txtCode.setText(highlightedContainer.toString());
    	ccd.setVisible(true);
    	return ccd.code;
    }
}
