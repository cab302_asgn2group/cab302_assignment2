package asgn2GUI;

import java.util.ArrayList;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.FreightContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * Creates a JPanel in which graphical components are laid out to represent the cargo manifest.
 *
 * @author Thuan Nguyen - N7195508.
 */
@SuppressWarnings("serial")
public class CargoCanvas extends JPanel {
    //Variables for the GUI
    private static final int WIDTH = 135;
    private static final int HEIGHT = 75;
    private static final int HSPACE = 12;
    private static final int VSPACE = 20;
    private static final int MARGIN_LEFT = 20;
    private static final int MARGIN_TOP = 15;
    private static final int XFILL = 8;
    private static final int YFILL = 3;
    
    //Cargo Fonts
    private static final String CARGOFONT = "Arial";
    private static final int CARGOFONT_SIZE = 14;
    
    //For toFindPos[] = {a, b} 
    //Where X is index of a, and Y is index of b
    private static final int X = 0;
    private static final int Y = 1;
    private static boolean mouseClick = false;
    private final CargoManifest cargo;
    
    //Information for images
    private final static String gSrc = "images/Containers/greyContainer.jpg";
    private final static String rSrc = "images/Containers/redContainer.jpg";
    private final static String bSrc = "images/Containers/blueContainer.jpg";
    
    private static BufferedImage greyContainer;
    private static BufferedImage redContainer;
    private static BufferedImage blueContainer;
    
    //Information about cargo
    private int numStacks = 0;
    private int maxStackHeight = 0;
    private ArrayList<FreightBox> freightBoxes;
    
    //Information to highlight a container
    private boolean highlightContainer = false;
    private ContainerCode toFind;
    private Integer[] toFindPos = {null, null};
    
    
    //Uber!
    private final static String uSrc = "images/Uber/uberCap.png";
    private static BufferedImage uberCap;

	/**
	 * Constructor for the canvas.
	 * 
	 * @author Thuan Nguyen - N7195508
	 * @param cargo The <code>CargoManifest</code> on which the graphics is based so that
	 * the number of stacks and height can be adhered to.
	 */
    public CargoCanvas(CargoManifest cargo) {
        freightBoxes = new ArrayList<FreightBox>();
        this.cargo = cargo;
        setBackground(Color.WHITE);
        setName("Canvas");
        setSize(WIDTH, HEIGHT);
        loadImages();
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent me) {
                //Highlight the FreightBox clicked, or disable highlight
                //if clicked elsewhere
                boolean highlighted = false;
                for (FreightBox fb : freightBoxes) {
                    if (fb.contains(me.getPoint())) {
                        mouseClick = true;
                        highlighted = true;
                        setToFind(fb.getContainer().getCode());
                    }
                }
                if(!highlighted){
                    disableHighlight();
                    repaint();
                }
                mouseClick = false;
            }
        });        
    }
    
    /**
     * Disables the highlighting of a container.
     * 
     * @author Thuan Nguyen - N7195508
     */
    public void disableHighlight(){
        this.toFind = null;
        this.highlightContainer = false;
        this.toFindPos[X] = null;
        this.toFindPos[Y] = null;
    }
    
    /**
     * Load Images for the containers
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void loadImages(){
        try {
            greyContainer = ImageIO.read(new File(gSrc));
            redContainer = ImageIO.read(new File(rSrc));
            blueContainer = ImageIO.read(new File(bSrc));  
        } catch (IOException e) {
            //If files do not exist, use default draw methods.
            greyContainer = null;
            redContainer = null;
            blueContainer = null;
        }        
        
        //A little fun thing here. Nothing important.
        try {
			uberCap = ImageIO.read(new File(uSrc));
		} catch (IOException e) {
	        uberCap = null;
		}
    }
    
    /**
     * Returns the code of the container being highlighted. <br>
     * Method helps to auto-fill the text field for unloading containers.
     * 
     * @author Thuan Nguyen - N7195508
     * @return The <code>ContainerCode</code> of the highlighted container
     */
    public ContainerCode getHighlightedContainer(){
        return this.toFind;
    }
    
    /**
     * Highlights a container with the specified <code>code</code>.
     * 
     * @author Thuan Nguyen - N7195508
     * @param code <code>ContainerCode</code> of the container to highlight.
     */
    public void setToFind(ContainerCode code) { 
        toFindPos[X] = cargo.whichStack(code);
        toFindPos[Y] = cargo.howHigh(code);

        if (!(toFindPos[X] == null || toFindPos[Y] == null)) {            
            this.toFind = code;
            this.highlightContainer = true;
        } 
        
        //Sets the scroll position when highlight is created by using the 
        //Find button
        if(!mouseClick){
            JScrollPane scrollPane = (JScrollPane) this.getParent().getParent().getParent();
            final int hCentre = scrollPane.getWidth()/2 - WIDTH/2;
            final int vCentre = scrollPane.getHeight()/2 - HEIGHT/2;      
            int horizontalPos = toFindPos[X] * WIDTH + MARGIN_LEFT - hCentre;
            int verticalPos = ((maxStackHeight - 1) - toFindPos[Y]) * HEIGHT + MARGIN_TOP 
                    - vCentre;
            
            scrollPane.getVerticalScrollBar().setValue(verticalPos);
            scrollPane.getHorizontalScrollBar().setValue(horizontalPos);
        }
        this.repaint();
    }

    /**
     * Draws the containers in the cargo manifest on the Graphics context of the Canvas.
     * 
     * @author Thuan Nguyen - N7195508
     * @param g The Graphics context to draw on.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g); 
        freightBoxes = new ArrayList<FreightBox>();
        if(cargo != null){
            Integer i = 0;
            try{
                cargoSize();        
                freightBoxes = freightBoxList();
                
                //Draws the base for allowable stacks
                while(true){                    
                    cargo.toArray(i);
                    g.fillRect(i * FreightBox.WIDTH + MARGIN_LEFT,
                            this.getHeight() - MARGIN_TOP,
                            FreightBox.WIDTH - XFILL, 4);
                    i++;
                }               
            } catch (ManifestException e) {
                //catch section here does nothing because the try catch block
                //was used to break out the loop when the index reaches a number 
                //that's greater than the amount of existing stacks on the cargo.
            }
            
            //A little fun thing. Runs when there are 0 stacks
            if(uberCap != null && i == 0){
        		final int pWidth = 496;
        		final int pHeight = 363;
        		g.setColor(Color.BLACK);
        		g.drawImage(uberCap, 
        				(getParent().getParent().getWidth() - pWidth)/2,
        				(getParent().getParent().getHeight() - pHeight)/2, 
        				pWidth,
        				pHeight,
        				null);
        	}                      
        } 
        
        //Draw all containers
        for(FreightBox fb : freightBoxes){
            fb.drawContainer(g);
        }
                
        if(highlightContainer){
            //Draw green frame around highlighted container
            Graphics2D g2d = (Graphics2D)g;
            final int LINEWEIGHT = 5;
            g2d.setStroke(new BasicStroke(LINEWEIGHT));     
            g2d.setColor(Color.GREEN);                      
            g2d.drawRect(toFindPos[X] * FreightBox.WIDTH + MARGIN_LEFT + LINEWEIGHT/2, 
                    this.getHeight() - ((toFindPos[Y] + 1) * FreightBox.HEIGHT + MARGIN_TOP 
                        - LINEWEIGHT/2),
                    FreightBox.WIDTH - FreightBox.XFILL - LINEWEIGHT,
                    FreightBox.HEIGHT - FreightBox.YFILL - LINEWEIGHT);
        }  
    }
    
    /**
     * Create a <code>FreightBox</code> item containing data on the type of container, its
     * x position, and y position, to be drawn on the <code>canvas</code>.
     * 
     * @author Thuan Nguyen - N7195508
     * @param container
     * @param xPos
     * @param yPos
     * @return <code>FreightBox</code> object
     */
    private FreightBox createFreightBox(FreightContainer container, int xPos, int yPos) {
        FreightBox fb = null;
        if (container instanceof DangerousGoodsContainer) {
            fb = new DangerousGoodsBox(container, xPos, yPos);
        }
        if (container instanceof GeneralGoodsContainer) {
            fb = new GeneralGoodsBox(container, xPos, yPos);
        }
        if (container instanceof RefrigeratedContainer) {
            fb = new RefrigeratedBox(container, xPos, yPos);
        }
        return fb;
    }
    
    /**
     * Finds the number of stacks and the highest stack, on the cargo.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private void cargoSize(){
        try{
            numStacks = 0;
            maxStackHeight = 0;
            while (true) {
                FreightContainer[] fcList = cargo.toArray(numStacks);
                if (fcList != null) {
                    maxStackHeight = (fcList.length > maxStackHeight) 
                            ? fcList.length : maxStackHeight;
                }
                numStacks++;
            }
        } catch (ManifestException e) {
            //catch section here does nothing because the try catch block
            //was used to break out the loop when the index reaches a number 
            //that's greater than the amount of existing stacks on the cargo.
        }
    }
    
    /**
     * Return an ArrayList of drawable <code>FreightBox</code> objects.<br>
     * Precondition: ArrayList of <code>FreightBox</code> is initially empty.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private ArrayList<FreightBox> freightBoxList(){
        ArrayList<FreightBox> freightBoxes = new ArrayList<FreightBox>();
        if(cargo != null){
            try{
                Integer i = 0;
                cargoSize();
                while(true){                    
                    FreightContainer[] fcList = cargo.toArray(i);
                    if(fcList != null){
                        //Fill ArrayList of freightBoxes with drawable containers
                        for(int j = 1; j <= fcList.length; j++){    
                            freightBoxes.add(createFreightBox(fcList[j-1], 
                                    i * FreightBox.WIDTH + MARGIN_LEFT, 
                                    this.getHeight() - j * FreightBox.HEIGHT
                                    - MARGIN_TOP));         
                        }
                    } 
                    i++;
                }
            } catch (ManifestException e) {
                //catch section here does nothing because the try catch block
                //was used to break out the loop when the index reaches a number 
                //that's greater than the amount of existing stacks on the cargo.
            }
        }
        return freightBoxes;
    }    
    
    /* (non-Javadoc)
     * @see javax.swing.JComponent#getPreferredSize()
     */
    @Override 
    public Dimension getPreferredSize(){
        Dimension dimension = new Dimension();
        
        if(cargo != null){
            cargoSize();
        }
        
        // Helps set canvas space proportionally to the number of stacks in
        // cargo and the highest stack
        int currentMaxWidth = numStacks * WIDTH + MARGIN_LEFT * 2 ;
        int currentMaxHeight = maxStackHeight * HEIGHT + MARGIN_TOP * 2;
        int defaultWidth = this.getParent().getParent().getWidth();
        int defaultHeight = this.getParent().getParent().getHeight();
        
        dimension.width = (currentMaxWidth > defaultWidth)
                ? currentMaxWidth : defaultWidth;
        dimension.height = (currentMaxHeight > defaultHeight)
                ? currentMaxHeight : defaultHeight;
                        
        return dimension;
    }  
    
    // drawContainer() is replaced by methods that are more customizable in FreightBox class
//
//        /**
//         * Draws a container at the given location.
//         *
//         * @param g The Graphics context to draw on.
//         * @param container The container to draw - the type determines the colour and ContainerCode
//         *            is used to identify the drawn Rectangle.
//         * @param x The x location for the Rectangle.
//         * @param y The y location for the Rectangle.
//         * @throws InvalidCodeException 
//         * @throws InvalidContainerException 
//         */
//        private void drawContainer(Graphics g, FreightContainer container, int x, int y){
//          g.fillRect(x, y, WIDTH - XFILL, HEIGHT - YFILL);
//          g.setColor(Color.WHITE);
//          g.setFont(new Font("Arial", Font.BOLD, 14));
//          g.drawString(container.getCode().toString(), x + HSPACE, y + VSPACE);
//          //Implementation here 
//          //Feel free to use some other method structure here, but this is the basis for the demo. 
//          //Obviously you need the graphics context and container as parameters. 
//          //But you can also use images if you wish. 
//        }
    
    /**
     * <code>FreightBox</code> class contains data for the container to be drawn.
     * 
     * @author Thuan Nguyen - N7195508
     */
    private abstract class FreightBox extends Rectangle2D{
        //Space for text positioning
        protected static final int HSPACE = CargoCanvas.HSPACE;
        protected static final int VSPACE = CargoCanvas.VSPACE;
        
        //Indication values of the mouse pointer position from the FreightBox
        private static final int OUT_LEFT = 1;
        private static final int OUT_TOP = 2;
        private static final int OUT_RIGHT = 4;
        private static final int OUT_BOTTOM = 8;
        
        //Fillet for width or height of container
        protected static final int XFILL = CargoCanvas.XFILL;
        protected static final int YFILL = CargoCanvas.YFILL;
        
        //x and y position on the area to be drawn
        protected int xPos;
        protected int yPos;
        
        //width and height of the container graphics
        protected static final int WIDTH = CargoCanvas.WIDTH;
        protected static final int HEIGHT = CargoCanvas.HEIGHT;
        
        private FreightContainer container;
        
        /**
         * Construct a container for drawing.
         * 
         * @author Thuan Nguyen - N7195508
         * @param container
         * @param xPos x-position on the canvas
         * @param yPos y-position on the canvas
         */
        protected FreightBox(FreightContainer container, int xPos, int yPos){
            this.xPos = xPos;
            this.yPos = yPos;
            this.container = container;
        }
        
        /**
         * Returns the <code>FreightContainer</code> object stored for this <code>FreightBox</code> 
         * @author Thuan Nguyen - N7195508
         * @return <code>FreightContainer</code> object
         */
        public FreightContainer getContainer(){
            return this.container;
        }

        /**
         * Draws the container with its information relavent to its type
         * 
         * @author Thuan Nguyen - N7195508
         * @param g
         */
        protected abstract void drawContainer(Graphics g);

        /* (non-Javadoc)
         * @see java.awt.geom.Rectangle2D#createIntersection(java.awt.geom.Rectangle2D)
         */
        @Override
        public Rectangle2D createIntersection(Rectangle2D r) {
            Rectangle2D tmp = new Rectangle2D.Double();
            Rectangle2D.intersect(this, r, tmp);
            return tmp;
        }

        /* (non-Javadoc)
         * @see java.awt.geom.Rectangle2D#createUnion(java.awt.geom.Rectangle2D)
         */
        @Override
        public Rectangle2D createUnion(Rectangle2D r) {
            Rectangle2D dest = new Rectangle2D.Double();
            Rectangle2D.union(this, r, dest);
            return dest;
        }

        /* (non-Javadoc)
         * @see java.awt.geom.Rectangle2D#outcode(double, double)
         */
        @Override
        public int outcode(double x, double y) {
            int out = 0;
            if(x < this.xPos){
                out |= OUT_LEFT;
            } else if(x > this.xPos + FreightBox.WIDTH - FreightBox.XFILL){
                out |= OUT_RIGHT;
            }
            
            if(y < this.yPos){
                out |= OUT_TOP;
            } else if(y > this.yPos + FreightBox.HEIGHT - FreightBox.YFILL){
                out |= OUT_BOTTOM;
            }
            return out;
        }

        /* (non-Javadoc)
         * @author Thuan Nguyen - N7195508
         * @see java.awt.geom.RectangularShape#contains(java.awt.geom.Point2D)
         */
        @Override
        public boolean contains(Point2D p){
            if(outcode(p.getX(), p.getY()) == 0){
                return true;
            }
            return false;
        }
        
        /* (non-Javadoc)
         * @see java.awt.geom.Rectangle2D#setRect(double, double, double, double)
         */
        @Override
        public void setRect(double x, double y, double w, double h) {
            this.xPos = (int) x;
            this.yPos = (int) y;
        }

        /* (non-Javadoc)
         * @see java.awt.geom.RectangularShape#getHeight()
         */
        @Override
        public double getHeight() {
            return HEIGHT;
        }

        /* (non-Javadoc)
         * @see java.awt.geom.RectangularShape#getWidth()
         */
        @Override
        public double getWidth() {
            return WIDTH;
        }

        /* (non-Javadoc)
         * @see java.awt.geom.RectangularShape#getX()
         */
        @Override
        public double getX() {
            return this.xPos;
        }

        /* (non-Javadoc)
         * @see java.awt.geom.RectangularShape#getY()
         */
        @Override
        public double getY() {
            return this.yPos;
        }

        /* (non-Javadoc)
         * @see java.awt.geom.RectangularShape#isEmpty()
         */
        @Override
        public boolean isEmpty() {
            return false;
        }   
    }

    /**
     * Drawable <code>FreightBox</code> for Refrigerated Containers
     * 
     * @author Thuan Nguyen - N7195508
     */
    private class RefrigeratedBox extends FreightBox{
        /**
         * Construct a Refrigerated Container for drawing
         * 
         * @author Thuan Nguyen - N7195508
         * @param container
         * @param xPos x-position on the canvas
         * @param yPos y-position on the canvas
         */
        protected RefrigeratedBox(FreightContainer container, int xPos, int yPos) {
            super(container, xPos, yPos);
        }

        /* (non-Javadoc)
         * @see asgn2GUI.CargoCanvas.FreightBox#drawContainer(java.awt.Graphics)
         */
        @Override
        protected void drawContainer(Graphics g) {
            RefrigeratedContainer container = (RefrigeratedContainer) this.getContainer();
            //Draw rectangle for refrigerated container
            g.setColor(Color.BLUE);
            if(blueContainer != null){
                g.drawImage(blueContainer, 
                        this.xPos,
                        this.yPos, 
                        FreightBox.WIDTH - FreightBox.XFILL, 
                        FreightBox.HEIGHT - FreightBox.YFILL, 
                        null);
            } else {
                g.fillRect(this.xPos, 
                        this.yPos, 
                        FreightBox.WIDTH - FreightBox.XFILL, 
                        FreightBox.HEIGHT - FreightBox.YFILL);
            }
            
            //Display information for refrigerated container
            g.setColor(Color.WHITE);
            g.setFont(new Font(CARGOFONT, Font.BOLD, CARGOFONT_SIZE));
            g.drawString(container.getCode().toString(), this.xPos + HSPACE, this.yPos + VSPACE);
            g.drawString("WT: " + container.getGrossWeight().toString(), 
                    this.xPos + HSPACE + 10,
                    this.yPos + VSPACE * 2);
            g.drawString("T: " + container.getTemperature().toString(), 
                    this.xPos + HSPACE + 10,
                    this.yPos + VSPACE * 3);
        }
        
    }

    /**
     * Drawable <code>FreightBox</code> for Dangerous Goods Containers
     * 
     * @author Thuan Nguyen - N7195508
     */
    private class DangerousGoodsBox extends FreightBox{
        /**
         * Construct a Dangerous Goods Container for drawing
         * 
         * @author Thuan Nguyen - N7195508
         * @param container
         * @param xPos x-position on the canvas
         * @param yPos y-position on the canvas
         */
        protected DangerousGoodsBox(FreightContainer container, int xPos, int yPos) {
            super(container, xPos, yPos);
        }

        /* (non-Javadoc)
         * @see asgn2GUI.CargoCanvas.FreightBox#drawContainer(java.awt.Graphics)
         */
        @Override
        protected void drawContainer(Graphics g) {
            DangerousGoodsContainer container = (DangerousGoodsContainer) this.getContainer();
            //Draw rectangle for dangerous goods container
            g.setColor(Color.RED);
            if(redContainer != null){
                g.drawImage(redContainer, 
                        this.xPos,
                        this.yPos, 
                        FreightBox.WIDTH - FreightBox.XFILL, 
                        FreightBox.HEIGHT - FreightBox.YFILL, 
                        null);
            } else {
                g.fillRect(this.xPos, 
                        this.yPos, 
                        FreightBox.WIDTH - FreightBox.XFILL, 
                        FreightBox.HEIGHT - FreightBox.YFILL);
            }
            //Display container information
            g.setColor(Color.WHITE);
            g.setFont(new Font(CARGOFONT, Font.BOLD, CARGOFONT_SIZE));
            g.drawString(container.getCode().toString(), this.xPos + HSPACE, this.yPos + VSPACE);
            g.drawString("WT: " + container.getGrossWeight().toString(), 
                    this.xPos + HSPACE + 10,
                    this.yPos + VSPACE * 2);
            g.drawString("C: " + container.getCategory().toString(), 
                    this.xPos + HSPACE + 10,
                    this.yPos + VSPACE * 3);
        }
    }

    /**
     * Drawable <code>FreightBox</code> for General Goods Container
     * 
     * @author Thuan Nguyen - N7195508
     */
    private class GeneralGoodsBox extends FreightBox{
        /**
         * Construct a container for drawing
         * 
         * @author Thuan Nguyen - N7195508
         * @param container
         * @param xPos x-position on the canvas
         * @param yPos y-position on the canvas
         */
        protected GeneralGoodsBox(FreightContainer container, int xPos, int yPos) {
            super(container, xPos, yPos);           
        }

        /* (non-Javadoc)
         * @see asgn2GUI.CargoCanvas.FreightBox#drawContainer(java.awt.Graphics)
         */
        @Override
        protected void drawContainer(Graphics g) {
            GeneralGoodsContainer container = (GeneralGoodsContainer) this.getContainer();
            //Draw rectangle for general goods container
            g.setColor(Color.GRAY);
            
            if(greyContainer != null){
                g.drawImage(greyContainer, 
                        this.xPos,
                        this.yPos, 
                        FreightBox.WIDTH - FreightBox.XFILL, 
                        FreightBox.HEIGHT - FreightBox.YFILL, 
                        null);
            } else {
                g.fillRect(this.xPos, 
                        this.yPos, 
                        FreightBox.WIDTH - FreightBox.XFILL, 
                        FreightBox.HEIGHT - FreightBox.YFILL);
            }
            
            //Display container information
            g.setColor(Color.WHITE);
            g.setFont(new Font(CARGOFONT, Font.BOLD, CARGOFONT_SIZE));
            g.drawString(container.getCode().toString(), 
                    this.xPos + HSPACE,
                    this.yPos + VSPACE);
            g.drawString("WT: " + container.getGrossWeight().toString(), 
                    this.xPos + HSPACE + 10,
                    this.yPos + VSPACE * 2);
        }   
    }
}

