package asgn2GUI;

import java.awt.Font;

import javax.swing.JTextArea;

import asgn2Codes.ContainerCode;
import asgn2Manifests.CargoManifest;

/**
 * Creates a JTextArea in which textual components are laid out to represent the cargo manifest.
 *
 * @author CAB302.
 */
@SuppressWarnings("serial")
public class CargoTextArea extends JTextArea {
    //Set initial size of text area;
	private static final int WIDTH = 120;
    private static final int HEIGHT = 50;
    
//    Set size for text space - COMMENTED BECAUSE NOT USED
//    private static final int HSPACE = 10;
//    private static final int VSPACE = 20;

    private final CargoManifest cargo;

    private ContainerCode toFind;

    /**
     * Constructor initialises the JTextArea.
     *
     * @author Thuan Nguyen - N7195508
     * @param cargo the <code>CargoManifest</code> on which the text area is based 
     */
    public CargoTextArea(CargoManifest cargo) {
        setFont(new Font("Calibri", Font.PLAIN, 12));
        setName("Cargo Text Area");
        setSize(WIDTH, HEIGHT);
        setEditable(false);
        this.cargo = cargo;
    }

    /**
     * Highlights a container.
     * 
     * @author Thuan Nguyen - N7195508
     * @param code ContainerCode to highlight.
     */
    public void setToFind(ContainerCode code) {
        this.toFind = code;
        this.setText(this.cargo.toString(this.toFind));
    }

    /**
     * Outputs the container representation from the cargo manifest on the text area.
     * 
     * @author Thuan Nguyen - N7195508
     */
    public void updateDisplay() {
    	this.setText(this.cargo.toString());
    }
}
