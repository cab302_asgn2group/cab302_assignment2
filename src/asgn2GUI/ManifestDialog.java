package asgn2GUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * Creates a dialog box allowing the user to enter parameters for a new <code>CargoManifest</code>.
 *
 * @author Thuan Nguyen - N7195508
 */
@SuppressWarnings("serial")
public class ManifestDialog extends AbstractDialog {
    //Size of Manifest Dialog box
	private static final int HEIGHT = 150;
    private static final int WIDTH = 250;

    private JTextField txtNumStacks;
    private JTextField txtMaxHeight;
    private JTextField txtMaxWeight;
    
    ActionListener listener;
    private CargoManifest manifest;

    /**
     * Constructs a modal dialog box that gathers information required for creating a cargo
     * manifest.
     *
     * @param parent the frame which created this dialog box.
     */
    private ManifestDialog(JFrame parent) {
        super(parent, "Create Manifest", WIDTH, HEIGHT);
        setName("New Manifest");
        setResizable(false);
    }

    /**
     * @see AbstractDialog.createContentPanel()
     */
    @Override
    protected JPanel createContentPanel() {
        txtNumStacks = createTextField(8, "Number of Stacks");
        txtMaxHeight = createTextField(8, "Maximum Height");
        txtMaxWeight = createTextField(8, "Maximum Weight");

        JPanel toReturn = new JPanel();
        toReturn.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        // Defaults
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.EAST;
        constraints.weightx = 100;
        constraints.weighty = 100;
        constraints.insets = new Insets(2, 3, 2, 3);

        addToPanel(toReturn, new JLabel(txtNumStacks.getName() + ": "), constraints, 0, 0, 2, 1);
        addToPanel(toReturn, new JLabel(txtMaxHeight.getName() + ": "), constraints, 0, 2, 2, 1);
        addToPanel(toReturn, new JLabel(txtMaxWeight.getName() + ": "), constraints, 0, 4, 2, 1);
        constraints.anchor = GridBagConstraints.WEST;
        addToPanel(toReturn, txtNumStacks, constraints, 3, 0, 2, 1);
        addToPanel(toReturn, txtMaxHeight, constraints, 3, 2, 2, 1);
        addToPanel(toReturn, txtMaxWeight, constraints, 3, 4, 2, 1);

        return toReturn;
    }

    /**
     * Factory method to create a named JTextField
     * @param numColumns
     * @param name
     * @return
     */
    private JTextField createTextField(int numColumns, String name) {
        JTextField text = new JTextField();
        text.setColumns(numColumns);
        text.setName(name);
        return text;
    }

    /* (non-Javadoc)
     * @see asgn2GUI.AbstractDialog#dialogDone()
     */
    @Override
    protected boolean dialogDone() {
    	try {
    	    //Checks for empty text fields
    	    if(txtNumStacks.getText().isEmpty()) {
    	        throw new NumberFormatException("No value has been entered for Number of Stacks.");
    	    }
    	    if(txtMaxHeight.getText().isEmpty()) {
                throw new NumberFormatException("No value has been entered for Max Height.");
            }
    	    if(txtMaxWeight.getText().isEmpty()) {
                throw new NumberFormatException("No value has been entered for Max Weight.");
            }
    	    
    		Integer numStacks = Integer.parseInt(txtNumStacks.getText());
    		Integer maxHeight = Integer.parseInt(txtMaxHeight.getText());
    		Integer maxWeight = Integer.parseInt(txtMaxWeight.getText());
    		manifest = new CargoManifest(numStacks, maxHeight, maxWeight);
    	} catch(NumberFormatException e){
    		JOptionPane.showMessageDialog(this, 
    				"Invalid Input: " + e.getMessage() + "\nInput must be an Integer.", 
    				"Input Error", JOptionPane.ERROR_MESSAGE);
    		return false;
    	} catch(ManifestException e){
    		JOptionPane.showMessageDialog(this, e.getMessage(), 
    				"Cargo Error", JOptionPane.ERROR_MESSAGE);
    		return false;
    	} 
    	return true;
    }
    
    /**
     * Shows the <code>ManifestDialog</code> for user interaction.
     *
     * @param parent - The parent <code>JFrame</code> which created this dialog box.
     * @return a <code>CargoManifest</code> instance with valid values.
     */
    public static CargoManifest showDialog(JFrame parent) {    	
    	ManifestDialog mg = new ManifestDialog(parent);
    	mg.setVisible(true);
    	return mg.manifest;
    }
}
